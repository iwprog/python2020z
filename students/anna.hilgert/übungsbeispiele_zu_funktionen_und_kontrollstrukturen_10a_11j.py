#Übungsbeispiele zu Funktionen und Kontrollstrukturen
#10.a) für die Berechnung von Herzfrequenz und Billetpreis

def berechne_herzfrequenz(age):
    return (220-age)



def get_billet_preis(age, distance):
    if age<6:
        return "0"
    elif age<16:
        return (2+distance*0.25)/2
    else:
        return 2+distance*0.25

eingabe = 1

while eingabe != 0:
    print ("=" * 50)
    print ("Programmübersicht:")
    print ("1 ... Berechnung der Herzfrequenz")
    print ("2 ... Preis für eine Fahrkarte berechnen")
    print (" " * 50)
    print ("0 ... Programm beenden")
    print ("=" * 50)
    print (" ")
    eingabe = int(input("Gewählte Option: "))
    
    if eingabe == 1:
        print (" ")
        print (" ")
        print ("Berechnung der Herzfrequenz")
        print ("-" * 35)
        age = int(input("Bitte geben Sie Ihr Alter ein:"))
        print (" ")
        print ("Ihre maximale Herzfrequenz beträgt: ", berechne_herzfrequenz(age), "Schläge pro Minute.")
        print (" ")
        
    elif eingabe == 2:
        print (" ")
        print (" ")
        print ("Preis für eine Fahrkarte berechnen")
        print ("-" * 35)
        age = int(input("Bitte geben Sie Ihr Alter ein:"))
        distance = int(input("Wie viele km möchten Sie reisen?"))
        print (" ")
        print ("Die Fahrkarte kostet: ", get_billet_preis(age, distance), " CHF")
        print (" ")
        
    elif eingabe == 0:
        print(" ")
        print("Programm beendet")
        print(" ")
        break
    
    else:
      print (" ")
      print ("Ungültige Option!")
      print (" ")  

#11.j)

raute = "# "
print (raute*11)
counter = 5
leer = 2
while counter > 0:
    print (" " * leer + raute * ((counter*2)-1))
    leer = leer + 2
    counter = counter -1






        