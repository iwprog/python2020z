#übungsbeispiele zu dictionaries
#aufgabe 2
l_input = "Der Tag begann sehr gut! Der Morgen war schön."

def word_stat(eingabe):
    statistik = {}
    eingabe = eingabe.lower()
    eingabe = eingabe.split()
    original_eingabe = list(eingabe)
    for word in original_eingabe:
        if word not in statistik:
            häufigkeit = eingabe.count(word)
            statistik[word] = häufigkeit
    return statistik
print (word_stat(l_input))

# hier müssten korrekterweise wohl noch die satzzeichen rausgefilter werden

#aufgabe 3
alphabet = {"A":"Alfa", "B":"Bravo", "C":"Charlie", "D":"Delta",
            "E":"Echo", "F":"Foxtrot", "G":"Golf", "H":"Hotel",
            "I":"India", "J":"Juliett", "K":"Kilo", "L":"Lima",
            "M":"Mike", "N":"November", "O":"Oscar", "P":"Papa",
            "Q":"Quebec", "R":"Romeo","S":"Sierra","T":"Tango",
            "U":"Uniform", "V":"Victor", "W":"Whiskey", "X":"X-ray",
            "Y":"Yankee", "Z":"Zulu"
            }

def buchstabieren():
    ausgabe = []
    eingabe = input("Welches Wort soll ich buchstabieren:")
    eingabe = eingabe.upper()
    for buchstabe in eingabe:
        if buchstabe not in alphabet.keys():
            print ("Kann", eingabe, "nicht buchstabieren, da",
                   buchstabe, "nicht definiert wurde.")
            break
        ausgabe.append((alphabet[buchstabe])+" - ")
    return ausgabe

print(buchstabieren())

# das mit der Auflistung mit Bindestrichen klappt nicht

#aufgabe 3b)

def buchstabieren():
    ausgabe = []
    eingabe = input("Welches Wort soll ich buchstabieren:")
    eingabe = eingabe.upper()
    for buchstabe in eingabe:
        if buchstabe == "Ä":
            buchstabe = "A"
            ausgabe.append((alphabet[buchstabe])+" - ")
            buchstabe = "E"
        if buchstabe == "Ö":
            buchstabe = "O"
            ausgabe.append((alphabet[buchstabe])+" - ")
            buchstabe = "E"
        if buchstabe == "Ü":
            buchstabe = "U"
            ausgabe.append((alphabet[buchstabe])+" - ")
            buchstabe = "E"
        ausgabe.append((alphabet[buchstabe])+" - ")
    return ausgabe

print(buchstabieren())