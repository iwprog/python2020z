# übungsbeispiele dicts
# 5 erweitert, eingegebene berge sollen sich vom programm gemerkt werden und in einer json datei gespeichert werden

dictionary= {}

a = 1

while True:
    berg = input(str(a)+". Berg: (oder x für beenden)")
    if berg == "x":
        break
    höhe = int(input(str(a)+". Höhe:"))
    dictionary[berg] = höhe
    a = a + 1

from json import loads, dumps

with open("berge.json", "w", encoding="utf8") as f:
    json_string = dumps(dictionary)
    f.write(json_string)

with open("berge.json", encoding="utf8") as f:
    json_string = f.read()
    dictionary = loads(json_string)

print(dictionary)
    