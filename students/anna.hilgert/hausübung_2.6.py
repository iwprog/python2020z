#Hausübung ad-hoc Übung 2.6
#Schreiben Sie ein Programm, welches drei Vierecke zeichnet


from turtle import *

reset()


def quadrat():
    pensize(5)
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)
    lt(90)
    fd(100)

#hier werden drei Vierecke gezeichnet
quadrat()
lt(60)
quadrat()
lt(60)
quadrat()

#hier sollen so viele Vierecke gezeichnet werden, wie man zu Beginn der Funktion eingibt

i = int(input("Wie viele Vierecke sollen gezeichnet werden? "))
gezeichnete_vierecke=0
while gezeichnete_vierecke<i:    
    print (quadrat(),lt(60))
    gezeichnete_vierecke=gezeichnete_vierecke+1
    
#klappt nicht:(