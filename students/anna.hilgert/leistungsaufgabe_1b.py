#Leistungsaufgabe 1b)
#Erweitern Sie das Programm um eine Eingabemöglichkeit (über Eingabefenster) für die Seitenlänge der Schweizer Flagge!
#Tipp: Achten Sie bei den Seiten- bzw. Formberechnungen darauf, dass die turtle-Anweisungen mit ganzen Zahlen parametrisiert werden. Hinweis: mit int() können Kommazahlen in ganze Zahlen konvertiert werden.

from turtle import *

reset()

def quadrat(sl):#Funktion rotes Quadrat definieren
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)
    return int(sl)

def kreuz(sl):#Funktion Kreuz definieren
    fd(sl)
    lt(90)
    fd(sl)
    rt(90)
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)
    rt(90)
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)
    rt(90)
    fd(sl)
    lt(90)
    fd(sl)
    lt(90)
    fd(sl)
    rt(90)
    fd(sl)
    return int(sl)

#rotes Quadrat zeichnen
sl=numinput("Eingabefenster", "Bitte Seitenlänge angeben:")
pencolor("red")
fillcolor("red")
begin_fill()
quadrat(sl)
end_fill()
#Stift zum Startpunkt der Kreuz-Zeichnung bewegen
home()
penup()
fd(int(sl/8*3))
lt(90)
fd(int(sl/8))
rt(90)
pendown()
#weiss gefülltes Kreuz zeichnen
fillcolor("white")
begin_fill()
kreuz(sl/4)
end_fill()
#Stift unsichtbar machen
hideturtle()