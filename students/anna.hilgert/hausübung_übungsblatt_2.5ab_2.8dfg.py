#Übungsblatt Grundlagen Kontrollstrukturen
#2.5 Verzweigungen
#a)
def legal_status(age):
    if age<18:
        return "minderjährig"
    else:
        return "volljährig"
age=int(input("Wie alt sind Sie?"))
print("Mit", age, "ist man", legal_status(age) + ".")
# 
# #b)
def legal_status(age):
    if age<0:
        return "ungeboren"
    elif age<=6:
        return "geschäftsunfähig"
    elif age<=14:
        return "unmündig"
    elif age<18:
        return "mündig minderjährig"
    else:
        return "volljährig"
age=int(input("Wie alt sind Sie?"))
print("Mit", age, "ist man", legal_status(age) + ".")

#2.8 Schleifen mit while
#d)
i=10
while i>0:
    print (i)
    i=i-1
print ("Start")

#f)

summe = 0
i=1
while summe <= 10:
    eingabe=int(input("Bitte geben Sie Zahl "+str(i)+" ein"))
    summe=summe+eingabe
    i=i+1
    
print("Summe: ",summe)

#g)

summe = 0
i=1
eingabe=int()

while eingabe != -1:
    eingabe=int(input("Bitte geben Sie Zahl "+str(i)+" ein"))
    i=i+1
    summe=summe+eingabe
    
print("Summe: ",summe+1)

    
