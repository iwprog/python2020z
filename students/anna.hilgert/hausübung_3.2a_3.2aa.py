# ad hoc 3.2 Seite 16
# a)

ergebnis = []
def allIndex(liste, element):
    for index in range(len(liste)):
        if liste[index] == element:
            ergebnis.append(index)
    return ergebnis


print (allIndex(["h","a","l","l","o"], "l"))


#ad hoc 3.2a Seite 17
#a)
einkaufsliste = ["Äpfel", "Birnen", "Butter", "Brot"]
print(sorted(einkaufsliste))

    
#b)
einkaufsliste = []

while True:
    artikel = input ("Was soll auf die Liste?")
    if artikel == "das wars":
        break
    einkaufsliste.append(artikel)

print (sorted(einkaufsliste))

#c)
einkaufsliste = [["Äpfel", 2.50], ["Birnen", 3.00], ["Butter", 1.50], ["Brot", 4.50]]
gesamtpreis = 0
for artikel, preis in einkaufsliste:
    print (artikel, "kostet", "CHF", preis)
    gesamtpreis = gesamtpreis + preis

print ("Der Einkauf kostet", gesamtpreis)

