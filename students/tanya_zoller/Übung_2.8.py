#Aufgabe a)
zahl = 0
while zahl <= 10:
    print(zahl)
    zahl = zahl + 1

#Aufgabe b)
zahl = 10
while zahl >= 0:
    print(zahl)
    zahl = zahl - 1

#Aufgabe c)
zahl = 10
while zahl >= 0:
    print(zahl)
    zahl = zahl - 3

#Aufagbe d)
zahl = 10
while zahl >= 0:
    if zahl == 0:
        print("Start")
    else:
        print(zahl)
    zahl = zahl - 1

#Aufgabe e)
zahl = -1
while zahl != 999999999999:
    zahl1 = int(input("Bitte eingeben: "))
    zahl2 = int(input("Bitte nächste Zahl eingeben: "))
    zahl3 = int(input("Bitte die letzte Zahl eingeben: "))
    print(zahl1 + zahl2 + zahl3)
    break

#Aufgabe f)
summe = 0
while summe <= 10:
    i = int(input("bitte eingeben"))
    summe = summe + i
print(summe)

#Aufgabe g)
summe = 0
while summe != 99999999:
    i = int(input("Zahl eingeben: "))
    if i == -1:
        break
    else:
        summe = summe + i
print(summe)
 