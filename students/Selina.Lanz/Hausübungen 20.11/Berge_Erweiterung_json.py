from json import loads, dumps

berge={}
for x in range (1,4):
    berg_eingabe=input(str(x) + ". Berg?")
    höhe_eingabe= input(str(x) + ". Höhe?")
    höhe_eingabe = int(höhe_eingabe)
    berge[berg_eingabe]=höhe_eingabe
    

# als json speichern
with open('berge.json', 'w', encoding='utf8') as f:
    json_string = dumps (berge)
    f.write(json_string)
    
# json einlesen
with open('berge.json', encoding='utf8') as f:
    json_string = f.read()
    berge = loads(json_string)