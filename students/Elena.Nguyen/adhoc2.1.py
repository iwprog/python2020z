# ad hoc 2.1
# Aufgabe a)

from turtle import *

shape("arrow")
pensize(5)
speed(6)


#Funktion definieren
def dreieck (seitenlaenge, stiftfarbe, fuellung):
    
    pencolor(stiftfarbe)
    fillcolor(fuellung)
    
    
    begin_fill()
    lt(120)
    fd(seitenlaenge)
    lt(120)
    fd(seitenlaenge)
    lt(120)
    fd(seitenlaenge)
    end_fill()


left(30)

dreieck(200, "red", "yellow")

right(120)

dreieck(200, "red", "lime")

right(120)

dreieck(200, "red", "cyan")


    
    