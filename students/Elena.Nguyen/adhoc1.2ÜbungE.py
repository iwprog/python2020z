#ad hoc Übung 1.2

#Übung E)

from turtle import *
reset()

shape("arrow")
pensize(5)
speed(6)

pencolor("blue")
rt(240)
fd(110)
lt(120)
fd(110)
lt(120)
fd(110)

pencolor("red")
fd(110)
lt(120)
fd(110)
lt(120)
fd(110)

pencolor("lime")
fd(110)
lt(120)
fd(110)
lt(120)
fd(110)

exitonclick()