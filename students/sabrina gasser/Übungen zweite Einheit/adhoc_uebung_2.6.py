from turtle import *

#hier einfach einen Sprung nach links, damit ich genügend Platz habe für die Zeichnung
penup()
back(300)
pendown()
speed(5)

def viereck(länge, winkel,stiftsprung):
    fd(länge)
    right(winkel)
    fd(länge)
    right(winkel)
    fd(länge)
    right(winkel)
    fd(länge)
    right(winkel)
    
    penup()
    fd(stiftsprung)
    pendown()
    
viereck(100,90,150)
viereck(100,90,150)
viereck(100,90,150)


anzahl_vierecke = numinput("Eingabefenster", "Wie viele Vierecke möchtest du zeichnen lassen?")

#weiss nicht weiter...? 