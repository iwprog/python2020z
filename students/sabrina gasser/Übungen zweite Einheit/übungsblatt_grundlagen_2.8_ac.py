#Sabrina Gasser
#21.09.2020

#2.8 Schleifen mit while
#a. Schreiben Sie eine Schleife, welche von 0 bis 10 zählt.


counter = 0
while counter <= 10:
    print(counter)
    counter = counter + 1



#c. Schreiben Sie eine Schleife, welche von 10 bis 0 in Dreierschritten zä̈hlt.
counter = 10

while counter >= 0:
    print(counter)
    counter = counter - 3
 
  
