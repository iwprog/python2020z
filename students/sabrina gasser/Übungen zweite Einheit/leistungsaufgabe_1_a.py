from turtle import *

speed(10)

#Leistungsaufgabe 1 a)

#Gehe ein wenig mehr in die Ecke oben links, damit man die Flagge guet sieht. 
penup()
back(200)
left(90)
fd(200)
right(90)
pendown()

#rotes Hintergrund-Quadrat 
begin_fill()
fillcolor("red")
pencolor("red")

fd(400)
right(90)
fd(400)
right(90)
fd(400)
right(90)
fd(400)
right(90)

end_fill()

#Hebe Stift um in den Innenteil zu wechseln und dort das Kreuz zu zeichnen
penup()
forward(240)
right(90)
forward(60) #60 von oben und 60 von unten. dadurch gleichmässig
pendown()

#Starte mit weissem Kreuz
begin_fill()
fillcolor("white")
pencolor("white")
forward(100)
left(90)
forward(100)
right(90)

forward(80) #seite rechts - habe nur 80 genommen, damit Kreuz ein wenig "schlanker" wirkt. Daher alle Seiten = 80

right(90)
forward(100)
left(90)
forward(100)
right(90)

forward(80)  #seite unten

right(90)
forward(100)
left(90)
forward(100)
right(90)

forward(80)  #seite links

right(90)
forward(100)
left(90)
forward(100)
right(90)

forward(80)  #seite oben

end_fill()

hideturtle()