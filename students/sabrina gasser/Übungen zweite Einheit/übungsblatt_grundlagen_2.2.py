#2.2 Funktionen ohne Rückgabewert mit Parameter
#Schreiben Sie eine Funktion, welche als Parameter einen Namen entgegen- nimmt und anschliessend den Text Guten Morgen Name! ausgibt.
#Sabrina Gasser
#21.09.2020

def guten_morgen(text):
    return "Guten Morgen" + text + "!"

print(guten_morgen(" Ana"))