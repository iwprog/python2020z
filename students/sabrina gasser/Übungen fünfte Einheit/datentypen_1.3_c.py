#a - nicht Teil der Hausaufgaben
print("--------")
print("Aufgabe a")
print("--------")
liste = ["Brot", "Zucker", "Salz", "Mehl", "Kerzen"]
liste_kuerzen = liste[:3]
print("Ergebnis:", liste_kuerzen)

#b - nicht Teil der Hausaufgaben
print("--------")
print("Aufgabe b")
print("--------")
liste = ["Brot", "Zucker", "Salz", "Mehl", "Kerzen"]
for einzelne_items in liste:
    if einzelne_items[-2:] != "er":
        print(einzelne_items)
        
#c
print("--------")
print("Aufgabe c")
print("--------")
liste = ["Brot", "Zucker", "Salz", "Mehl", "Kerzen", "Ei", "Mi", "Mo", "Mu"]
for einzelne_items in liste:
    if len(einzelne_items) >= 3:
        print(einzelne_items)
        