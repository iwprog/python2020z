#Autor: Sabrina Gasser
#Datum: 14.09.2020

from turtle import *

reset()

pencolor("red")
pensize(5)

#Ich habe die Abbilung doppelt erstellt. Einmal hier Teil 1 bei welchem die Seitenlängen fix sind ohne Numinput

fillcolor("cyan")
begin_fill()
right(90)
forward(100)
left(120)
forward(100)
left(120)
forward(100)
end_fill()

fillcolor("yellow")
begin_fill()
forward(200)
left(120)
forward(200)
left(120)
forward(200)
end_fill()

fillcolor("lime")
begin_fill()
forward(50)
left(120)
forward(50)
left(120)
forward(50)
end_fill()

#Stift heben und Sprung nach rechts für Teil 2, in welchem ich Numinput verwende.
left(90)
penup()
forward(300)
pendown()

#Hier mit numinput. Ich habe Numinput für Cyan & Lime verwendet. Yellow ist in meinem Beispiel unveränderbar.
laenge = numinput("Eingabefenster", "Bitte Seitenlänge angeben:")

fillcolor("cyan")
begin_fill()
right(90)
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()


fillcolor("yellow")
begin_fill()
forward(200)
left(120)
forward(200)
left(120)
forward(200)
end_fill()

fillcolor("lime")
begin_fill()
forward(laenge)
left(120)
forward(laenge)
left(120)
forward(laenge)
end_fill()