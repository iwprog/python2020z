# b) einkaufsliste
#     - erstellen sie ein programm, das eine einkaufsliste verwaltet, welche über die folgenden funktionen verfügt:
#        
#     (1) artikel hinzufügen
#     (2) artikel löschen
#     (3) artikel suchen
#     (4) einkaufsliste leeren
#     (5) einkaufsliste speichern
#     (6) einkaufsliste laden
#     (7) einkaufsliste im csv format exportieren
#     (0) exit
#    
#     - zu jedem artikel sollen preis und menge gespeichert werden.
#     - die funktion "artikel suchen" soll alle artikel ausgeben,
#     die den suchbegriff enthalten.
#       (der suchbegriff 'en' würde zum beispiel 'birnen', 'kirschen'
#        und 'erdbeeren' auswählen).
#     - es soll möglich sein, die einkaufsliste ins csv format zu
#     exportieren (z.b. als einkaufsliste.csv)

einkaufsliste = {}

while True:

    print(20*'--')
    print('(1) artikel hinzufügen')
    print('(2) artikel löschen')
    print('(3) artikel suchen')
    print('(4) einkaufsliste leeren')
    print('(5) einkaufsliste speichern')
    print('(6) einkaufsliste laden')
    print('(7) einkaufsliste im csv format exportieren')
    print('(0) exit')
    print(20*'--')

    eingabe = int(input('Bitte Nr wählen: '))
    
    if eingabe == 0:
        break
    
    elif eingabe == 1:
        
        artikel = input("Bitte füge einen Artikel hinzu: ")
        preis = input("Preis? ")
        menge = input("Menge? ")
        dictionary = {"Preis": preis, "Menge": menge}
        einkaufsliste[artikel] = dictionary
        for artikel, dictionary in einkaufsliste.items():
            print("Artikel: ", artikel, ", CHF", dictionary["Preis"], ", Menge:", dictionary["Menge"], "mal")
    
    elif eingabe == 2:
        artikel_löschen = input('welchen artikel willst du löschen? ')
        if artikel_löschen in einkaufsliste:
            del einkaufsliste[artikel_löschen]
        print("Artikel: ", artikel, ", CHF", dictionary["Preis"], ", Menge:", dictionary["Menge"], "mal")
    
    elif eingabe == 3: #funktioniert nicht.... Gibt mir immer das letzte aus.
        artikel_suchen = input('welchen artikel suchst du? ')
        if artikel_suchen in einkaufsliste:
            print("hier ist dein Artikel: ", artikel, ", CHF", dictionary["Preis"], ", Menge:", dictionary["Menge"], "mal")
   
    elif eingabe == 4:
        einkaufsliste.clear()
        
    elif eingabe == 5:
         with open('einkaufsliste.txt', 'w', encoding='utf8') as f:
            el_save = f.write(str(einkaufsliste))
#musste str davor machen, weil Meldung kam --- TypeError: write() argument must be str, not dict
            print("deine Einkaufsliste wurde als .txt gespeichert")
    elif eingabe == 6:
         with open('einkaufsliste.txt', encoding='utf8') as f:
            el_read = f.read()
            print("hier deine Einkaufsliste:", el_read)
    elif eingabe == 7:
        from csv import reader
        with open ('einkaufsliste.csv', 'w', encoding='utf8') as f:
            csv_writer = writer(f, delimiter=';')
            #geht nicht. wo ist der Fehler...?
            for zeile in einkaufsliste: 
                print(zeile)
                csv_writer.writerow(zeile)
                print("es wurde eine .csv gespeichert")
                