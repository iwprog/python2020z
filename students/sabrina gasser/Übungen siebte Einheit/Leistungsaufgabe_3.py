#a
liste=[]
while True:
    stadt = input("Geben Sie die Stadt ein: ")
    if stadt == "x":
        break
    ez = input("Geben Sie die zugehörige Einwohnerzahl ein: ")
    liste.append([stadt, ez])
print(liste)  #für den Teil bzgl "normal" ausgeben
print(sorted(liste)) #für den Teil bzgl alphabetisch sortiert ausgeben
    

#b  
wörter = {"Haus": "house", "Zahnarzt": "dentist", "Zahn": "teeth",
        "Maus": "mouse", "Hund": "dog", "Zug": "train", "Auto": "car",
        "Velo": "bike", "Welt": "world", "Meer": "sea"
        }

#funktioniert nur halb - hört nach dem letzten Wort "Meer" nicht auf, sondern beginnt wieder von vorne....
i = 0
richtig = 0   
while True:
    for deutsch, englisch in wörter.items():
        eingabe = input(("Was heisst " + deutsch+ "?"))
        if deutsch == "Meer":  #ziel wäre hier gewesen: sobald das deutsche Wort "Meer" kommt, sollte es stoppen... geht aber nicht
            print("Sie haben", richtig, "von 10 Vokabeln richtig beantwortet!")
            break
        elif eingabe == englisch:
            i = i + 1
            richtig = richtig + 1
            print("RICHTIG")
        elif eingabe != englisch:
            i = i + 1
            print("FALSCH")