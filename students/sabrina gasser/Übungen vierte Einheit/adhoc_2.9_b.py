from turtle import *

eingabe = numinput("eingabefenster", "wie viele dreiecke soll ich zeichnen...? ")

speed(5)

def zeichnung(füllung):
    fillcolor(füllung)
    
    begin_fill()

    for x in range(3):
        fd(100)
        left(120)
    end_fill()

    #hinweis für mich : Schleife in Aufgabe 2.9a hatte ich mit "while" gemacht, hier mit "in range ()"
    
    
def abstand(sl):
    penup()
    forward(120)
    pendown()
    
for anzahl_dreiecke in range(int(eingabe)):   #in die Klammer "eingabe" reinschreiben, wo vorher schon die exakte Anzahl definiert war
    zeichnung("blue")
    abstand(100)