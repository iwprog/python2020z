#Grundlagen Datentypen 2.3 c)

#entfernen, welche Zeichenkette en im Namen haben

preisliste = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
preislistezwei = dict(preisliste)

preisliste["Tee"] = 4.2
preisliste["Peanuts"] = 3.9
preisliste["Ketchup"] = 2.1
preisliste["Ei"] = 1.5

for lebensmittel, preis in preislistezwei.items():
    if "en" in str(lebensmittel):
        del preisliste[lebensmittel]
        
print(preisliste)



