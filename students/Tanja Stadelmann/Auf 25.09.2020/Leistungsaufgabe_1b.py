from turtle import *
#Leistungsaufgabe 1, Schweizerflagge a)

size = numinput("Abfragedialog",
"Bitte Seitenlänge angeben:")

def viereck_gross (zeichenfarbe, füllfarbe):
    pencolor(zeichenfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    forward(size/10)
    right(90)
    forward(size/10)
    right(90)
    forward(size/10)
    right (90)
    forward (size/10)
    end_fill()
    
def rechteck_klein (zeichenfarbe, füllfarbe):
    pencolor(zeichenfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    forward(size/20)
    right(90)
    forward(size/20)
    right(90)
    forward(size/20)
    right(90)
    forward(size/20)
    end_fill()

#Viereck gross
viereck_gross ("red", "red")

penup()
right(90)
forward(size/10)
pendown()

#Rechteck klein

rechteck_klein("red", "red")

penup()
right(90)
forward(size/20)
pendown()

#Viereck gross
viereck_gross ("red", "red")

penup()
right(90)
forward(size/10)
right(90)
forward(size/10)
pendown()

#Rechteck klein
rechteck_klein("red", "red")

penup()
right(90)
forward(size/20)
pendown()

#Viereck gross
viereck_gross ("red", "red")

penup()
right(90)
forward(size/10)
right(90)
forward(size/10)
pendown()

#Rechteck klein
rechteck_klein("red", "red")

penup()
right(90)
forward(size/20)
pendown()

#Viereck gross
viereck_gross ("red", "red")

penup()
right(90)
forward(size/10)
right(90)
forward(size/10)
pendown()

#Rechteck klein
rechteck_klein("red", "red")
