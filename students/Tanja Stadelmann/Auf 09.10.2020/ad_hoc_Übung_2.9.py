#ad hoc Übung 2.9 b)

from turtle import*
reset()


anzahl = input("Wie viele Dreiecke sollen gezeichnet werden?")
anzahl = int(anzahl)

def dreieck(sl):
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    
def abstand(sl):
    penup()
    fd(sl+50)
    pendown()
    
for anzahl_dreiecke in range(anzahl):
    dreieck(100)
    abstand(100)
    
#c) Anzahl pro Reihe und wie viele Reihen
    
from turtle import*
reset()


anzahl = input("Wie viele Dreiecke sollen in einer Reihe gezeichnet werden?")
anzahl = int(anzahl)

reihe = input("Wie viele Reihen sollen gezeichnet werden?")
reihe = int(reihe)

def dreieck(sl):
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    fd(sl)
    lt(120)
    
def abstand(sl):
    penup()
    fd(sl+50)
    pendown()
    
for anzahl_dreiecke in range(anzahl):
    dreieck(100)
    abstand(100)
    
penup()
backward(200*anzahl)
rt(90)
forward(100)
lt(90)
pendown()
    
for reihe_dreiecke in range(reihe):
    dreieck(100)
    abstand(100)
