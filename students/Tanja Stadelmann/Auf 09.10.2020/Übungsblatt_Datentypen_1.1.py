#Übungsblatt Datentypen: 1.1

liste_jahreszeiten = ["Frühling", "Sommer", "Herbst", "Winter"]

#b) Frühling aus Liste löschen

del liste_jahreszeiten[0]

print(liste_jahreszeiten)

#c) Langas hinzufügen

liste_jahreszeiten.append("Langas")

print(liste_jahreszeiten)

#d) Namen einer Liste hinzufügen

name_liste = []

eingabe = str(input("Name:"))

while eingabe != "x":
    eingabe = input("Name:")
    
    if eingabe == "x":
        name_liste.append(eingabe)
        print(name_liste)
        
        
#Hier nimmt es mir immer nur den letzten Eintrag = x in die Liste!
