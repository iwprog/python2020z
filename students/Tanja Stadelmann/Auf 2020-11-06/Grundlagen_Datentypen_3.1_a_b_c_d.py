#Grundlagen Datentypen, 3.1 - a, b, c, d

#a)

datensatz = {"Ana":["Skupch", "123"]}

for vorname, liste in datensatz.items():
    nachname, phone = liste
    print(datensatz)

#b)

telefonbuch = []

telefonbuch.append([vorname, nachname, phone])

print(telefonbuch)

#c) Datensätze hinzufügen

telefonbuch.append(["Tim", "Kurz", "732"])
telefonbuch.append(["Juli", "Lang", "912"])

print(telefonbuch)

#d) Personendaten eingeben

while True:
    vorname = input("Geben Sie einen Vornamen ein, oder x für Exit")
    if vorname.lower() == "x":
        break
    nachname = input("Geben Sie den Nachnamen ein")
    telefon = input("Geben Sie die Telefonnummer ein")

    telefonbuch.append([vorname, nachname, telefon])
        
print(telefonbuch)









