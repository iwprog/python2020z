#Grundlagen Datentypen, 3.2 a)

#Datensatz von 3.1

datensatz = {"Ana":["Skupch", "123"]}

for vorname, liste in datensatz.items():
    nachname, phone = liste
    print(datensatz)

telefonbuch = []

telefonbuch.append([vorname, nachname, phone])

telefonbuch.append(["Tim", "Kurz", "732"])
telefonbuch.append(["Juli", "Lang", "912"])

#Eigentliche Aufgabe a)

for vorname, nachname, phone in telefonbuch:

    print("Vorname: ", vorname)
    print("Nachname: ", nachname)
    print("Phone: ", phone)


