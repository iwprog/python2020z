#author sarah brandenberger
#date 29.10.2020
#hausuebung 7

from turtle import *
from math import *
from random import *


# übungsblatt: grundlagen datentypen 3.1 a

mein_dicitionary = {"Vorname" : "Ana", "Nachname": "Skupch", "Phone":123}

# übungsblatt: grundlagen datentypen 3.1 b

telefonbuch = {"Ana":["Skupch", 123]}


# übungsblatt: grundlagen datentypen 3.1 c

telefonbuch["Tim"] = ["Kurz", 732]
telefonbuch["Julia"] = ["Tusek", 912]

print(telefonbuch)

# übungsblatt: grundlagen datentypen 3.1 d

telefonbuch = {}

while True:
    vorname = input ("Geben Sie einen Vornamen ein")

    while vorname != "x":
        nachname = input ("Geben Sie einen Nachnamen ein")
        phone = input ("Geben Sie eine Telefonnummer ein")
        telefonbuch[vorname] = [nachname, phone]
        print("Vorname:", vorname, "\n" + "Name:", nachname + "\n" + "Phone:", phone)
        break
    if vorname == "x":
        break


#übungsbeispiele beispiel 5

berge = {}

for i in range (3):
        bergname = input("Geben Sie den Bergnamen ein")
        höhe = input ("Geben Sie den Höhe ein")
        berge[bergname] = höhe
        höhe = float(höhe)
        for bergname, höhe in sorted(berge.items()):
            höhe = float(höhe)
            print (bergname, "ist", höhe, "m (" + str(int(höhe * 3.28)) + " ft) hoch")
 

#übungsbeispiele beispiel 6

berge = {}

for i in range (3):
    bergname = input("Geben Sie den Bergnamen ein")
    höhe = input ("Geben Sie den Höhe ein")
    gebirge = input ("Geben Sie den Gebirgenamen ein")
    berge[bergname] = [höhe, gebirge]
for i in range (1):
        print("Möchten Sie Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge?")
        sortieren_1 = int(input())
        if sortieren_1 == 1:
            sortieren_1 = "Berg"
            for bergname, höhe, gebirge in sorted(berge.items()):
                print (bergname, "ist", höhe, "m  hoch und gehört zum", gebirge, "Gebirge")
        if sortieren_1 == 2:
            sortieren_1 = "Höhe"
            sortierliste = []
            for bergname, [höhe, gebirge]  in sorted(berge.items()):
                sortierliste.append([höhe, bergname, gebirge])  
            for höhe, bergname, gebirge in sorted(sortierliste):
                print(bergname, "ist", höhe, "m  hoch und gehört zum", gebirge, "Gebirge")
        if sortieren_1 == 3:
            sortieren_1 = "Gebirge"
            sortierliste = []
            for bergname, [höhe, gebirge] in sorted(berge.items()):
                sortierliste.append([gebirge, bergname, höhe])
            for gebirge, bergname, höhe in sorted(sortierliste):
                print(bergname, "ist", höhe, "m  hoch und gehört zum", gebirge, "Gebirge")
            break
for i in range (1):
    print ("Möchten Sie", sortieren_1, "(1) aufsteigend oder (2) absteigend sortieren?")  
    sortieren_2 = int(input())
    if sortieren_2 == 1:
        for bergname, höhe, gebirge in sorted(berge.items()):
                print (bergname, "ist", höhe, "m  hoch und gehört zum", gebirge, "Gebirge")
    if sortieren_2 == 2:
        for bergname, [höhe, gebirge] in sorted(berge.items(), reverse=True):
                print (bergname, "ist", höhe, "m  hoch und gehört zum", gebirge, "Gebirge")
        


#übungsbeispiele beispiel 7

eingabetext = "David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen."
blacklist =  ("david", "mayer", "htw", "chur")

def schwärzen(eingabetext, blacklist):
    eingabetext = eingabetext.lower()
    eingabetext_neu = []
    for wort in eingabetext.split():
        if wort not in blacklist:
            eingabetext_neu.append(wort)
        elif wort in blacklist:
            länge = len(wort)       
            eingabetext_neu.append("****")
    return eingabetext_neu
        
print(schwärzen(eingabetext, blacklist))



 
#datei öffnen

# with open('gutenberg.txt', encoding='utf8') as fileHandle:
#     for line in fileHandle:
#         print(line)
# 
# 
# line = line.split()
# 
# print ("Himmel kommt", (line.count("Himmel")), "vor")
# print ("Freiheit kommt", (line.count("Freiheit")), "vor")
# print ("Spiel kommt", (line.count("Spiel")), "vor")
# print ("Tanz kommt", (line.count("Tanz")), "vor")
# 

        
