#author sarah brandenberger
#date 14.11.2020
#hausuebung 8

from turtle import *
from math import *
from random import randint
from urllib.request import urlopen
from csv import *
from json import loads, dumps

# #ad-hoc übung 6.1

zähler = 1
with open("hausuebung_8.txt", "r", encoding="utf-8") as filehandling:    
            hausuebung = filehandling.readline()
            for zeile in filehandling:
                print(zähler,":", hausuebung)    
                zähler = zähler + 1

try:
            with open("hausuebung_8a.txt", "w", encoding="utf-8") as filehandling:
                hausuebung_sichern = filehandling.write(hausuebung)
                print(zähler, ":", hausuebung)
    
    
except FileNotFoundError:
        print("die Datei konnte nicht gefunden werden")
    
except:
         print("es ist ein Fehler aufgetaucht")
     
     
# #übungsaufgaben zu dictionaries
     
berge = {}

   
def merken():
    for i in range (3):
        bergname = input("Geben Sie den Bergnamen ein")
        höhe = input ("Geben Sie den Höhe ein")
        berge[bergname] = höhe
        höhe = float(höhe)
        for bergname, höhe in sorted(berge.items()):
            höhe = float(höhe)
            print (bergname, "ist", höhe, "m (" + str(int(höhe * 3.28)) + " ft) hoch")
            berge_return = bergname, "ist", höhe, "m (" + str(int(höhe * 3.28)) + " ft) hoch"
            with open('berge.json', "w", encoding='utf8') as berge_sichern:
                werte = dumps(berge_return) 
                berge_sichern.write(werte)
            try:
                with open('berge.json', "r", encoding='utf8') as berge_laden:
                    werte = berge_laden.read() 
                    berge_return = loads(werte)
            except FileNotFoundError:
                print("die Datei konnte nicht gefunden werden") 
merken()

#einkaufsliste

einkaufsliste = {}

while True:
    print("Drücken Sie", "\n", "1 zum Artikel hinzufügen", "\n", "2 Artikel löschen", "\n", "3 Artikel suchen", "\n", "4 Einkaufsliste leeren", "\n", "5 Einkaufsliste speichern", "\n", "6 Einkaufsliste laden", "\n", "7 Einkaufsliste im csv format exportieren" "\n", "0 zum beenden des Programms")
    wahl = int(input())
    if wahl == 1:
        artikel = input("Geben Sie den Artikel ein")
        preis = input("Geben Sie den Preis ein")
        menge = input("Geben Sie eine Menge ein")
        einkaufsliste[artikel] = [preis, menge]
        preis = float(preis)
        menge = int(menge)
        print("Sie haben", menge, "Stück", artikel, "zum Preis von CHF", preis, "hinzugefügt", "Ihre Einkaufsliste enthält:", einkaufsliste)
        continue
    elif wahl == 2:
        löschen = input("Geben Sie den Artikel, den Sie löschen möchten ein")
        löschen = löschen.lower()
        if löschen in einkaufsliste:
            del einkaufsliste[löschen]
        print("Sie haben", löschen, "gelöscht, Ihre Einkaufsliste enthält:", einkaufsliste_neu) 
#         einkaufsliste_neu = {}
#         if löschen not in einkaufsliste:
#             einkaufsliste_neu[artikel] = [preis, menge]   
#         print("Sie haben", löschen, "gelöscht, Ihre Einkaufsliste enthält:", einkaufsliste_neu)   
        continue
    elif wahl == 3:
        suche = input("Geben Sie einen Suchbegriff ein")
        suche = suche.lower()
        umwandeln = str(einkaufsliste)
        einkaufsliste = eval(umwandeln)
        if suche in einkaufsliste:
            print(suche, "kommt auf Ihrer Einkaufsliste vor,", "Ihre Einkaufsliste enthält:", einkaufsliste)
        else: 
                print(suche, "kommt nicht auf Ihrer Einkaufsliste vor,", "Ihre Einkaufsliste enthält:", einkaufsliste)  
#         def einkaufsliste_suchen():
#             suchliste = {}
#             for artikel in einkaufsliste.items():
#                 if suche in artikel:
#                     suchliste[artikel] = [preis, menge]
#             return suchliste
#             print(suchliste, "Ihre Suche ist in enthalten")
#              
#         einkaufsliste_suchen()
       
    elif wahl == 4:
        einkaufsliste.clear()
        print("Sie haben die Einträge in der Einkaufsliste geleert, Ihre Einkaufsliste enthält:", einkaufsliste)
        continue   
    elif wahl ==5:
        with open ("einkaufsliste.txt", "w", encoding="utf8") as filehandling:
            einkaufsliste_sichern = filehandling.write(str(einkaufsliste))
            print("Ihre Einkaufsliste wurde als txt-File einkaufsliste gespeichert")               
    elif wahl ==6:
        with open ("einkaufsliste.txt", "r", encoding="utf8") as filehandling:
                einkaufsliste_laden = filehandling.read()
                print("Ihre Einkaufsliste enthält:", einkaufsliste_laden)
    elif wahl == 7:
        with open('einkaufsliste_csv.csv', 'w', encoding='utf8') as csv_anlegen:
            csv_writer = writer(csv_anlegen, delimiter=';')
            csv_writer.writerow(einkaufsliste)   
        print("Eine Excel Datei wurde geschrieben")           
    elif wahl ==0:
        break
                
    

    
        
    
# 
# 