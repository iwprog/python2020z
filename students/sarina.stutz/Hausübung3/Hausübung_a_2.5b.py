#Autor:Sarina Stutz
#Datum:30.09.2020
#Übungsblatt basic loops
#Übungen 2.5 b

def legal_status(age):
    if age <=-1:
        print ("Mit", age, "ist man ungeboren.")
    elif age <=6:
        print ("Mit", age, "ist man geschäftsunmündig.")
    elif age <=14:
        print ("Mit", age, "ist man unmündig.")
    elif age <18:
        print ("Mit", age, "ist man mündig minderjährig.")
    else:
        print ("Mit", age, "ist man volljährig.")
    
legal_status(-8)
legal_status(5)
legal_status(7)
legal_status(15)
legal_status(18)





