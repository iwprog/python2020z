#Autor:Sarina Stutz
#Datum:30.09.2020
#Übungsblatt basic loops
#Übungen 2.5 a 

#wenn age grösser 17, dann volljährig, sonst minderjährig
def legal_status(age):
    if age <= 17:
        print ("Mit", age, "ist man minderjährig")
    else:
        print ("Mit", age, "ist man volljährig")
    
legal_status(22)
legal_status(17)



