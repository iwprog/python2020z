#Autor:Sarina Stutz
#Datum:24.09.2020
#ad hoc Übung 2.1 a)

from turtle import *
reset()

#Stiftfarbe und Stiftdicke für alle gleich
pencolor("red")
pensize(5)

#Dreiecke mittels Funktionen
def dreieck (laenge, fuellfarbe):
    
    fillcolor(fuellfarbe)
    begin_fill()
    forward(laenge)
    right(120)
    forward(laenge)
    right(120)
    forward(laenge)
    end_fill()
    
right(150)   
dreieck(100, "yellow")
right(250)
dreieck(100, "lightgreen")
right(240)
dreieck(100, "cyan")
reset ()