#Autorin:Sarina Stutz
#Datum:21.10.2020
#Übungsblatt: basic Data Types
#Übungen 2.2 a, e


#2.2 a)
preisliste = {"Brot":3.2,"Milch":2.1,"Orangen":3.75,"Tomaten":2.2}
preisliste["Tee"]=4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1
for lebensmittel,preis in preisliste.items():
    print(lebensmittel,"kostet",preis, "CHF")
    
#2.2 e)
preisliste2 ={"Brot":3.2, "Schokolade":1.5, "Bananen":1}
for lebensmittel, preis in preisliste2.items():
    if preis < 2.0:
        print(lebensmittel)



