#Autorin:Sarina Stutz
#Datum:5.11.2020
#Übungsblatt: dict
#Übung 5

berge={}
for x in range (1,4):
    berg_eingabe=input("Berg?")
    höhe_eingabe= input("Höhe?")
    höhe_eingabe = int(höhe_eingabe)
    berge[berg_eingabe]=höhe_eingabe
    
sortierliste = []
for berg, hoehe in berge.items():
    sortierliste.append([berg,hoehe])
for berg, hoehe in sorted(sortierliste):
    print (berg, "ist", hoehe, "m", "(",round(hoehe*3.28),"ft.)", "hoch.")






