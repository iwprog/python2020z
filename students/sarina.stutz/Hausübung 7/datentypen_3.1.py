#Autorin:Sarina Stutz
#Datum:4.11.2020
#Übungsblatt: basic Data Types
#Übungen 3.1 a, b, c, d


#3.1 a) Eintrag Ana

ana ={'Vorname':'Ana','Nachname':'Skupch','Phone':123}
#3.1 b) hinzufügen zu Liste Telefonbuch
telefonbuch = [ana]
print (telefonbuch)

#3.1c) 2 Einträge zur Liste hinzufügen
tim={'Vorname':'Tim','Nachname':'Kurz','Phone':732}
julia={'Vorname':'Julia','Nachname':'Lang','Phone':912}
telefonbuch.append(tim)
print (telefonbuch)
telefonbuch.append(julia)
print (telefonbuch)



#3.1d)
while True: #endlosschleife, bis x eingegeben wird.
    vorname = input ("Geben Sie einen Vornamen/x ein: ")
    if vorname.lower()=="x":
        break
    nachname = input ("Geben Sie einen Nachnamen ein: ")
    phone = input ("Geben Sie die Phone-Nr. ein: ")
    telefonbuch.append ({"Vorname": vorname, "Nachname": nachname, "Phone": phone})
print (telefonbuch)



