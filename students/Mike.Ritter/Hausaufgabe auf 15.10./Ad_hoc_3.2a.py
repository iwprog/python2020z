# ad hoc Übung 3.2

# Teilaufgabe a)

einkaufsliste = ["Äpfel","Birnen","Butter","Brot"]
einkaufsliste.sort()
print(einkaufsliste)

# Teilaufgabe b)

liste_einkauf=[]
while True:
    artikel = input ("Was möchten Sie einkaufen:")
    if artikel == "nichts":
        break
    liste_einkauf.append(artikel)
print (liste_einkauf)

# Teilaufgabe c)

einkauf = [["Nüsse", 5], ["Brot", 3.50], ["Zwiebeln", 3]]
for artikel, preis in einkauf:
    print(artikel, "kostet", preis, "Fr.")
resultat=einkauf[0][1] + einkauf [1][1] + einkauf [2][1]
print("Der Gesamtpreis beträgt:",resultat,"Fr")

