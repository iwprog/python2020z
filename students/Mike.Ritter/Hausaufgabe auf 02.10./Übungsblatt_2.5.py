# Übungsblatt 2.5

# Teilaufgabe a)

def legal_status(age):
    if age<18:
        return "minderjährig"
    else:
        return "volljährig"
age=int(input("Wie alt sind Sie?"))
print("Mit", age, "ist man", legal_status(age) + ".")

# Teilaufgabe b)

def legal_status(age):
    if age<0:
        return "ungeboren"
    elif age<=6:
        return "geschäftsunfähig"
    elif age<=14:
        return "unmündig"
    elif age<18:
        return "mündig minderjährig"
    else:
        return "volljährig"
age=int(input("Wie alt sind Sie?"))
print("Mit", age, "ist man", legal_status(age) + ".")

