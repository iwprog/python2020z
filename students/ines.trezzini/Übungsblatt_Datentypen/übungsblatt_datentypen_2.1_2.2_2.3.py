# Übungsblatt Datentypen

# 2.1a)

preisliste={"Brot": 3.2, "Milch": 2.1, "Orangen": 3.75, "Tomaten": 2.2}
print(preisliste)

# 2.1b)
preisliste["Milch"]=2.05
print(preisliste)

# 2.1c)
del preisliste["Brot"]
print(preisliste)

# 2.1d)
preisliste["Tee"]=4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1
print(preisliste)


# 2.1e)
dictionary_lebensmittel={}
lebensmitteleingabe = 0
while lebensmitteleingabe !="x":
    lebensmitteleingabe=input("Lebensmittel: ")
    if lebensmitteleingabe=="x":
        break
    else:
        preiseingabe=input("Preis: ")
    dictionary_lebensmittel[lebensmitteleingabe]=preiseingabe
print(dictionary_lebensmittel)

# 2.2a)
# Variante 1:
produktliste={"Milch": "2.1", "Brot": "3.2"}
for produkt in produktliste:
    print(produkt,"kostet",produktliste[produkt],"CHF.")

# Variante 2: ist aber nicht so hübsch
print(produktliste.items())
for produkt,preis in produktliste.items():
    print(produkt,"kostet", preis,"CHF.")

# 2.2e): als Sätze ausgeben
produktliste2={"Zucker":0.9,"Milch":2.1, "Brot":3.2,"Butter":1.8, "Wasser":0.8, "Honig":0.95, "Birne":2.55}
for produkt in produktliste2:
    if produktliste2[produkt]<2.0:
        print(produkt,"kostet",produktliste2[produkt],"CHF.")

# Variante: nur Produkte ausgeben
produktliste2={"Zucker":0.9,"Milch":2.1, "Brot":3.2,"Butter":1.8, "Wasser":0.8, "Honig":0.95, "Birne":2.55}
for produkt in produktliste2:
    if produktliste2[produkt]<2.0:
        print(produkt)

# Variante: in Dictionary ausgeben, dazu Produkte zu neuem Dictionary hinzufügen:
produktliste2={"Zucker":0.9,"Milch":2.1, "Brot":3.2,"Butter":1.8, "Wasser":0.8, "Honig":0.95, "Birne":2.55}
leer_dictionary={}
for produkt,preis in produktliste2.items(): # zwei Werte, daher braucht man Tupel (.items)
    if preis<2.0:
        leer_dictionary[produkt]=preis
print(leer_dictionary)

# 2.3c)
produktliste3={"Senf":0.9,"Milch":2.1, "Blumenkohl":3.15, "Rosenkohl": 6.40, "Brot":3.2,"Bohnen":2.55}

for produkt in list(produktliste3):
    if "en" in produkt:
        produktliste3.pop(produkt)
print(produktliste3)

# 2.3c): Variante
preisliste_2 = {"Brot":3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
# besser neue Liste machen statt Kopie und Löschung
def preisliste_filtern():
    resultat={}
    for item, preis in preisliste_2.items(): # zwei Werte, daher braucht man Tupel (.items)
        if "en" not in item:
            resultat[item]=preis
    return resultat

print(preisliste_filtern())
   
# Achtung: Wenn return, "dann reitet Python in den Sonnenuntergang und wird nie wieder gesehen".
# D.h.: Was nach return steht, wird nicht ausgeführt
      