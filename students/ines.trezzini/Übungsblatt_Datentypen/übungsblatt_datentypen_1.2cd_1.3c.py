# # 1.2c)
# Geben Sie alle Jahreszeiten und zusätzlich die letzten zwei Buchstaben der Jahreszeit aus.
jahreszeiten=["Frühling","Sommer","Herbst","Winter"]

def endbuchstaben_jahreszeiten():
    for element in jahreszeiten:
        print(element,"-",element[-2:])

endbuchstaben_jahreszeiten()

# Ohne Funktion:
jahreszeiten=["Frühling","Sommer","Herbst","Winter"]

for element in jahreszeiten:
    print(element,"-",element[-2:])

# 1.2d) Geben Sie nur jene Jahreszeiten aus, die mit den Buchstaben "er" enden.
# auf vorheriger Aufgabe aufbauend, denn wir wissen: die gesuchten Endungen ergeben sich durch element[-2:]
jahreszeiten=["Frühling","Sommer","Herbst","Winter"]

# ????noch ergänzen
def jahreszeitenendungen():
    for element in jahreszeiten:
        if element[-2:]=="er":
            print(element)

jahreszeitenendungen()

# Variante mit Löschen, falls "er" an einer beliebigen Position auftritt:
jz = ["Frühling", "Sommer", "Herbst", "Winter"]

original_list = list(jz)
for element in original_list:
  if 'er' in str(element):
    jz.remove(element)

print(jz)

# Ohne Funktion:
jahreszeiten=["Frühling","Sommer","Herbst","Winter"]

for element in jahreszeiten:
    if element[-2:]=="er":
        print(element)
        
# 1.3c) Schreiben Sie eine Funktion, welche aus einer Liste alle Texte entfernt, welche weniger als drei Buchstaben haben.
liste3=["ich","du","er","sie","es","wir","ihr","sie"]
 
# Länge der ganzen Liste: print(len(liste3))
# Länge der einzelnen Elemente: for element in liste3: print(len(element))
# man muss mit unveränderter Liste arbeiten, da remove (Liste verändert sich laufend, Elemente verändern ihre Position etc.)
for element in liste3:
    unveränderte_liste=list(liste3)
    for element in unveränderte_liste:
        if len(element)<3:
            liste3.remove(element)
print(liste3)

# Variante, die nicht zuverlässig funktioniert ...:
# for element in liste3:
#     if len(element)<3:
#         liste3.remove(element)
# print(liste3)
# "er" wird auf diese Art und Weise nicht gelöscht, da die Liste sich während der Bearbeitung verändert!