# ad hoc 3.2a): allIndex

def allIndex(liste, element):
    zahlenliste=[]
    for position in range (len(liste)):
        if liste[position]==element:
           zahlenliste.append(position)
    if len(zahlenliste) == 0:
        return -1
    return zahlenliste
   
print(allIndex(['H', 'a', 'l', 'l', 'o'], "l"))
print(allIndex(['H', 'a', 'l', 'l', 'o'], "t"))

# ad hoc 3.2aa): Einkaufsliste
einkaufsliste = ["Aepfel","Birnen","Butter","Brot"] # Äpfel wird nicht korrekt sortiert, nur Aepfel.

def sortierte_einkäufe():
    print(sorted(einkaufsliste))
   
sortierte_einkäufe()

# ad hoc 3.2ab): Einkaufsliste

def sortierte_einkäufe2():
    print(sorted(einkaufsliste2))

print("Einkaufsliste anlegen.")
print("Drücken Sie x zum Beenden und Sortieren der Liste.")

eingabetext=input("Einkauf eingeben: ")
einkaufsliste2=[eingabetext]

while eingabetext !="x":
    eingabetext=input("Einkauf eingeben: ")
    einkaufsliste2.append(eingabetext)
    if eingabetext=="x":
        einkaufsliste2.remove("x") # das "x" soll nicht in der Liste auftauchen 
        sortierte_einkäufe2()

# Variante ohne Funktion
meine_einkäufe=[]
eingabe = 0
print("Einkaufsliste anlegen.")
print("Drücken Sie x zum Beenden und Sortieren der Liste.")

while eingabe !="x":
    eingabe=input("Einkauf eingeben: ")
    meine_einkäufe.append(eingabe)
    if eingabe=="x":
        meine_einkäufe.remove("x") # das "x" soll nicht in der Liste auftauchen
        print(sorted(meine_einkäufe))
        
# ad hoc 3.2ac): Einkaufsliste
einkauf_mit_preis = [["Apfel",0.75],["Birnen",2.00],["Butter",1.80],["Brot",2.50],]
for produkt, preis in einkauf_mit_preis:
    print(produkt+", Preis:", preis, "Fr.")
summe=einkauf_mit_preis[0][1]+einkauf_mit_preis[1][1]+einkauf_mit_preis[2][1]+einkauf_mit_preis[3][1]
print("Der Einkauf kostet insgesamt",summe,"Fr.")
