# Beispiel 2)

l_input = "Der Tag begann sehr gut! Der Morgen war schön."

def word_stat(text):
    text=text.split() # aus dem Text wird eine Liste von Wörtern kreiert
    worthäufigkeit = {}
    for wort in text: # "Der","Tag","begann" ...
        wort=wort.lower() # "der","tag","begann" ...
        if wort not in worthäufigkeit:
            worthäufigkeit[wort]=0
        worthäufigkeit[wort]=worthäufigkeit[wort]+1
#       andere Möglichkeit: worthäufigkeit[wort]+=1 (# ergoogelt ...)
    print(worthäufigkeit)

word_stat(l_input)

# Variante:
def wortstatistik(l_input):
    input_list = l_input.lower().split()
    anzahl_woerter = {}
    for word in input_list:
        anzahl_W = input_list.count(word)
        anzahl_woerter[word] = anzahl_W
    print(anzahl_woerter)
        
wortstatistik("Der Tag begann sehr gut! Der Morgen war schön.")

# Beispiel 3a): geht nur mit Kleinbuchstaben
ICAO={"a":"Alfa", "b":"Bravo", "c":"Charlie", "d":"Delta",
      "e":"Echo","f":"Foxtrot","g":"Glof","h":"Hotel", "i":"India",
      "j":"Juliett", "k":"Kilo", "l":"Lima","m":"Mike",
      "n":"November", "o":"Oscar", "p":"Papa", "q":"Quebec",
      "r":"Romeo","s":"Sierra","t":"Tango","u":"Uniform","v":"Victor",
      "w":"Whiskey","x":"X-Ray","y":"Yankee","z":"Zulu"}

wort = input("Welches Wort soll ich buchstabieren? ")
ergebnis = ""
for position, x in enumerate(wort):
    ergebnis = ergebnis + ICAO[x] + "-"

print(ergebnis[:-1])

# Beispiel 3b)
ICAO={"a":"Alfa", "b":"Bravo", "c":"Charlie", "d":"Delta",
      "e":"Echo","f":"Foxtrot","g":"Glof","h":"Hotel", "i":"India",
      "j":"Juliett", "k":"Kilo", "l":"Lima","m":"Mike",
      "n":"November", "o":"Oscar", "p":"Papa", "q":"Quebec",
      "r":"Romeo","s":"Sierra","t":"Tango","u":"Uniform","v":"Victor",
      "w":"Whiskey","x":"X-Ray","y":"Yankee","z":"Zulu"}

wort = input("Welches Wort soll ich buchstabieren? ")
wort = wort.lower().replace("ä","ae").replace("ö","oe").replace("ü","ue")

ergebnis = []
for position, x in enumerate(wort):
    ergebnis.append(ICAO[x])

# join befehl: Trennzeichen (z.b. '-') und im Anschluss eine Liste von
# Elementen, die "gejoined" werden soll.
print("-".join(ergebnis))

# Beispiel 3a) nicht ganz richtig
icao={'a':'alfa', 'b':'bravo', 'c':'charlie', 'd':'delta', 'e':'echo', 
     'f':'foxtrot', 'g':'golf', 'h':'hotel', 'i':'india', 'j':'juliett', 
     'k':'kilo', 'l':'lima', 'm':'mike', 'n':'november', 'o':'oscar', 
     'p':'papa', 'q':'quebec', 'r':'romeo', 's':'sierra', 't':'tango', 
     'u':'uniform', 'v':'victor', 'w':'whiskey', 'x':'x-ray', 'y':'yankee', 
     'z':'zulu'}

print("----")

worteingabe = 0
worteingabe=input("Welches Wort soll ich buchstabieren? ")
worteingabe=worteingabe.lower()
buchstabe = worteingabe.split()

for buchstabe in worteingabe:
    if buchstabe in icao:
        print(icao[buchstabe],end="-")
    else:
        print("Kann",worteingabe,"nicht buchstabieren, da",buchstabe, "nicht definiert wurde.")
        break      

# Wie letzten Bindestrich entfernen?
