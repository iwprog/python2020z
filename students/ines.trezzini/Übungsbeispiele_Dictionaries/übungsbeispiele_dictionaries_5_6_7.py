# Übungsbeispiele Dictionaries

# Beispiel 5
berge={}
zählung=1
while zählung<4:
    name = input(str(zählung)+". Berg: ")
    höhe = input(str(zählung)+". Höhe: ")
    berge[name]=höhe
    zählung=zählung+1
print(berge)

for name, höhe in sorted(berge.items()):
    print(name,"ist",höhe,"m ("+str(float(höhe)*float(3.28))+" ft) hoch.")

# Beispiel 5, Variante 1:
print("Variante zu Aufgabe 5")

berge={}
eingabe=1
zählung=1
while eingabe<4:
    name = input(str(zählung)+". Berg: ")
    höhe = input(str(zählung)+". Höhe: ")
    berge[name]=höhe
    zählung=zählung+1
    eingabe=eingabe+1
print(berge)


höhe=float(höhe)
sortierte_berge = []
for name, höhe in berge.items():
    sortierte_berge.append([name, höhe])

for name, höhe in sorted(sortierte_berge):
    print(name,"ist",höhe,"m ("+str(float(höhe)*float(3.28))+" ft) hoch.")

# Beispiel 5, Variante 2:
berge={}
for x in range (1,4):
    berg_eingabe=input(str(x) + ". Berg?")
    höhe_eingabe= input(str(x) + ". Höhe?")
    höhe_eingabe = int(höhe_eingabe)
    berge[berg_eingabe]=höhe_eingabe
    
for berg, höhe in sorted(berge.items()):
    print(berg, "ist", höhe, "m (" + str(höhe*3.28), "ft) hoch.")
    
# Beispiel 6: Berge mit Gebirge
dictionary= {}
angaben = {}

# x = 0
# a = 1
# 
# while x < 3:
#     berg = input(str(a)+". "+"Berg:")
#     höhe = int(input(str(a)+". "+"Höhe:"))
#     gebirge = input(str(a)+". "+"Gebirge")
#     angaben = {"Höhe": höhe, "Gebirge": gebirge}
#     dictionary[berg] = angaben
#     a = a + 1
#     x = x + 1

dictionary = {"Everest": {"Höhe": 8848, "Gebirge": "Him"},
              "Arlberg": {"Höhe": 2000, "Gebirge": "Alpen"},
              "ZCalander": {"Höhe": 2500, "Gebirge": "Alpen"}}

sortieren_nach = int(input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge:"))
aufoderab = int(input("Nach Berg (1) aufsteigend, oder (2) absteigend sortieren?:"))
if aufoderab == 1:
    reverse = False
else:
    reverse = True

if sortieren_nach == 1:
    for berg, angaben in sorted(dictionary.items(), reverse=reverse):
        höhe, gebirge = angaben
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
            
elif sortieren_nach == 2:
    sortierliste = []
    for berg, angaben in dictionary.items():
        sortierliste.append([angaben["Höhe"], berg, angaben])
    
    for höhe, berg, angaben in sorted(sortierliste):
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
        
elif sortieren_nach == 3:
    sortierliste = []
    for berg, angaben in dictionary.items():
        sortierliste.append([angaben["Gebirge"], berg, angaben])
    
    for höhe, berg, angaben in sorted(sortierliste):
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")

# Beispiel 6: Berge mit Gebirge (Variante):

dictionary= {}
angaben = {}

# x = 0
# a = 1
# 
# while x < 3:
#     berg = input(str(a)+". "+"Berg:")
#     höhe = int(input(str(a)+". "+"Höhe:"))
#     gebirge = input(str(a)+". "+"Gebirge")
#     angaben = {"Höhe": höhe, "Gebirge": gebirge}
#     dictionary[berg] = angaben
#     a = a + 1
#     x = x + 1

def get_hoehe(item):
    return item[1]["Höhe"]

def get_gebirge(item):
    return item[1]["Gebirge"]


dictionary = {"Everest": {"Höhe": 8848, "Gebirge": "Him"},
              "Arlberg": {"Höhe": 2000, "Gebirge": "Alpen"},
              "ZCalander": {"Höhe": 2500, "Gebirge": "Alpen"}}

sortieren_nach = int(input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge:"))
aufoderab = int(input("Nach Berg (1) aufsteigend, oder (2) absteigend sortieren?:"))
if aufoderab == 1:
    reverse = False
else:
    reverse = True

if sortieren_nach == 1:
    for berg, angaben in sorted(dictionary.items(), reverse=reverse):
        höhe, gebirge = angaben
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
            
elif sortieren_nach == 2:
    for berg, angaben in sorted(dictionary.items(), key=get_hoehe):
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")
    
        
elif sortieren_nach == 3:
    sortierliste = []
    for berg, angaben in sorted(dictionary.items(), key=get_gebirge):
        print(berg, "ist", angaben["Höhe"], "m hoch und gehört zum", angaben["Gebirge"], "Gebirge.")


# Beispiel 7 (schöne Ausgabe):
eingabetext = "David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen."
blacklist = "david, mayer, htw, chur"

def stopword_filter(eingabetext,blacklist):
    Ausgabe = {}
    eingabetext=eingabetext.lower()
    eingabetext=eingabetext.split()
# .split(), damit aus dem Dictionary eine Liste wird
    blacklist = blacklist.lower()
    blacklist = blacklist.split(", ")
# .split(), damit aus dem Dictionary eine Liste wird, getrennt anhand des Kommas
    for wort in eingabetext:
        if wort in blacklist:
            Ausgabe[wort]=len(wort)*"*"
        else:
            Ausgabe[wort]=wort
    geschwärzter_satz = " ".join(Ausgabe.values())
    print("Ausgabe:", geschwärzter_satz) 

stopword_filter(eingabetext,blacklist)

# Beispiel 7 (Beispiel von Gitlab):
ueberschreibung = {}
eingabetext = input("Wie lautet ihr Eingabetext?").lower()
eingabetext = eingabetext.split()
blacklist = input("Was soll auf die Blacklist?").lower()
blacklist = blacklist.split(", ")

ausgabe = []
for wort in eingabetext:
    if wort in blacklist:
        ausgabe.append("*"*len(wort))
    else:
        ausgabe.append(wort)

satz = " ".join(ausgabe)
print ("Ausgabe:", satz)


# Beispiel 7: nicht ganz richtige Ausgabe
eingabetext = "David Mayer von der HTW hat um 9:00 seine Wohnung in Chur verlassen."
blacklist = "david, mayer, htw, chur"

def stopword_filter(eingabetext,blacklist):
    Ausgabe = []
    eingabetext=eingabetext.lower()
    eingabetext=eingabetext.split()
# .split(), damit es eine Liste wird
    for wort in eingabetext:
        if wort in blacklist.lower():
            wort=len(wort)*"*"
            Ausgabe.append(wort)
        else:
            Ausgabe.append(wort)
    print(Ausgabe) 

stopword_filter(eingabetext,blacklist)

# nicht ganz richtige Variante von Beispiel 7 mit Input:
eingabetext=input("Eingabe: ")
blacklist=input("Blacklist: ")

def stopword_filter(eingabetext,blacklist):
    ausgabe = []
    eingabetext=eingabetext.lower()
    eingabetext=eingabetext.split()
    for wort in eingabetext:
        if wort in blacklist.lower():
            wort=len(wort)*"*"
            ausgabe.append(wort)
        else:
            ausgabe.append(wort)
    print("Ausgabe: ",ausgabe)

stopword_filter(eingabetext,blacklist)