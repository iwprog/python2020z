# ad hoc 6.1

# 1. Schreiben sie ein Programm, das eine Sicherungskopie einer Datei erstellt.

# Erstellen der zu sichernden Datei (Text in Datei schreiben)

with open("zu_sichernde_datei.txt","w",encoding="utf8") as f:
    f.write("Das ist der Anfang.")
    f.write("\n")
    f.write("Das ist die Mitte.\n")
    f.write("Das ist der Schluss\n")

# Die zu sichernde Datei ("zu_sichernde_datei.txt") wird einmal gedruckt, damit man sieht, was drinsteckt:
print("---Öffnen der Originaldatei---")

f=open("zu_sichernde_datei.txt", "r", encoding="utf8")
originaltext=f.read()
print("Originaldatei: ", originaltext)
f.close()

# Verkürzte Variante:
print("---Öffnen der Originaldatei: verkürzte Variante!---")

with open("zu_sichernde_datei.txt",encoding="utf8") as f:
    originaltext2=f.read()
print("Variante der Originaldatei: ",originaltext2)

# Die Sicherungskopie wird erstellt und geöffnet:
print("---Erstellung und Öffnen der Sicherungsdatei---")

f=open("gesichterte_datei.txt", "w", encoding="utf8")
kopiedatei=f.write(originaltext)
f.close()

f=open("gesichterte_datei.txt", "r", encoding="utf8")
kopiedatei_geöffnet=f.read()
print("Originaldatei: ", kopiedatei_geöffnet)
f.close()

# Verkürzte Variante:
print("---Erstellung und Öffnen der Sicherungsdatei: verkürzte Variante---")

with open("gesichterte_datei.txt", "w", encoding="utf8") as f:
    kopiedatei=f.write(originaltext)
    
with open("gesichterte_datei.txt", "r", encoding="utf8") as f:
    kopiedatei=f.read()
print("Kopierte Originaldatei: ", kopiedatei)

# 2. Verändern Sie das Programm so, dass in der Zieldatei bei jeder Zeile zuerst die Zeilennummer gefolgt von einem Doppelpunkt steht.
# Soll die Ausgabe nummeriert werden oder der Inhalt der Datei?
# Wie kann ich ab 1 starten? Ergoogelt: enumerate(x,start=1)
# Warum gibt es einen Abstand zwischen den Zeilen? Ergoogelt: zeile = zeile.strip("\n")
print("---Das habe ich leider nicht ganz verstanden ...---")

with open("gesichterte_datei2.txt", "w", encoding="utf8") as x:
    kopiedatei2=x.write(originaltext)

with open("gesichterte_datei2.txt", encoding='utf8') as x:
    for nummer,zeile in enumerate(x,start=1):
        zeile = zeile.strip("\n")
        print(str(nummer)+":",zeile)

# 3. Erweitern Sie das Programm, sodass es eine Fehlermeldung ausgibt, wenn die Datei nicht geöffnet (oder geschrieben) werden konnte.
print("\n---Programmerweiterung---")
print("---try, except---")

try:

    with open("ungesichterte_datei.txt",encoding="utf8") as f:
        kopiedatei=f.read()
    print("Kopierte Originaldatei: ", kopiedatei)
    
except FileNotFoundError:
    print("Die Datei kann nicht gefunden werden.")
except PermissionError:
    print("Die Datei kann nicht geöffnet werden.")
