# ad-hoc 1.2, Seite 18

from turtle import *

# a) Quadrat
reset()
forward(100)
left(90)
forward(100)
left(90)
forward(100)
left(90)
forward(100)

# weiter zur nächsten Zeichnung
penup()
forward(120)
right(270)
pendown()

# b) Viereck
forward(200)
left(90)
forward(100)
left(90)
forward(200)
left(90)
forward(100)

# weiter zur nächsten Zeichnung
penup()
forward(100)
right(270)
pendown()

# c) Zickzack-Linie
pencolor("blue")
pensize(5)
left(50)
forward(100)

pencolor("red")
pensize(5)
right(100)
forward(100)

pencolor("cyan")
pensize(5)
left(100)
forward(100)

pencolor("black")
pensize(5)
right(100)
forward(100)

# weiter zur nächsten Zeichnung
right(100)
penup()
forward(230)
right(210)
pendown()

# d) Dreieck
pencolor("red")
pensize(5)
forward(100)
left(120)
forward(100)
left(120)
forward(100)

# weiter zur nächsten Zeichnung
penup()
backward(500)
left(120)
pendown()

# e) Windrad
pensize(5)
# erstes Dreieck
pencolor("red")
forward(100)
left(120)
forward(100)
left(120)
forward(100)
# zweites Dreieck
pencolor("green")
forward(100)
left(120)
forward(100)
left(120)
forward(100)
# drittes Dreieck
pencolor("blue")
forward(100)
left(120)
forward(100)
left(120)
forward(100)