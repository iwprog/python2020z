# ad-hoc 1.1.a), Seite 13:
# Stellen Sie mit der print-Anweisung einen Datensatz aus folgenden einzelnen Bestandteilen zusammen: Name, Vorname, Strasse, Hausnr., PLZ, Wohnort

# Versuch Variante 1
Name="Trezzini,"
Vorname="Ines,"
Strasse="Attinghauserstrasse"
Hausnummer="30,"
PLZ=6460
Wohnort="Altdorf UR"
print(Name, Vorname, Strasse, Hausnummer, PLZ, Wohnort)

# Versuch Variante 2
print("Trezzini, Ines, Attinghauserstrasse 30, 6460 Altdorf UR")

# Versuch Variante 3
print("Trezzini,","Ines,","Attinghauserstrasse 30,","6460","Altdorf UR")

# ad-hoc 1.1.b), Seite 13:
# Schreiben Sie einen Befehl, der alle Zahlen von 1 bis 5 addiert, subtrahiert, multipliziert und dividiert

print("Addition:", 1+2+3+4+5)
print("Subtraktion:", 1-2-3-4-5)
print("Multiplikation:", 1*2*3*4*5)
print("Division:", 1/2/3/4/5)

# ad-hoc 1.1.c), Seite 13:
# Schreiben Sie einen Befehl, der den Satz "Wenn Fliegen hinter ..." ausgibt und dabei so kurz wie möglich ist!
print("wenn fliegen hinter","fliegen "*6+"nach.")

# ad-hoc 1.1.d), Seite 13:
# Schreiben Sie einen Befehl, der den String "Hello" rückwärts ausgibt

# Variante 1:
print("Hello"[-1]+"Hello"[-2]+"Hello"[-3]+"Hello"[-4]+"Hello"[-5])

# Variante 2:
txt = "Hello" [: : -1]
print(txt)
