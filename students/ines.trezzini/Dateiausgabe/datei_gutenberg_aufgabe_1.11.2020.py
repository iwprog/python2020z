# https://www.gutenberg.org/cache/epub/6079/pg6079.txt

# Datei öffnen:
from urllib.request import urlopen

with urlopen("https://www.gutenberg.org/ebooks/6079.txt.utf-8") as source:
    web_content=source.read().decode("utf8")
    print(web_content)

# Wörter zählen:
# Variante 1:
#     for word in web_content.split():
    wörter=web_content.split()
    print(wörter.count("Himmel"))
    print(wörter.count("Freiheit"))
    print(wörter.count("Spiel"))
    print(wörter.count("Tanz"))
#     Nur ganze Wörter gezählt

# Variante 2:
    print(web_content.count("Himmel"))
    print(web_content.count("Freiheit"))
    print(web_content.count("Spiel"))
    print(web_content.count("Tanz"))
#     Hier werden auch zusammengesetzte Wörter gezählt

# Variante 3:
i=0
j=0
x=0
y=0

for wörter in web_content.split():
    if wörter=="Himmel":
        i=i+1
    if wörter=="Freiheit":
        j=j+1
    if wörter=="Spiel":
        x=x+1
    if wörter=="Tanz":
        y=y+1
print("Himmel kommt",str(i)+"-mal vor.")
print("Freiheit kommt",str(j)+"-mal vor.")
print("Spiel kommt",str(x)+"-mal vor.")
print("Tanz kommt",str(y)+"-mal vor.")

# Varianten zu dieser Aufgabe: alles ergoogelt ...
print("-------------------------------------------------")

# Datei öffnen:
f = open("pg6079.txt", "r", encoding="utf8")
print(f.read())

import webbrowser
webbrowser.open("pg6079.txt")

# Wörter zählen:
#get file object reference to the file
file = open("pg6079.txt", "r")

#read content of file to string
data = file.read()

#get number of occurrences of the substring in the string
occurrences1 = data.count("Himmel")
print("Das Wort 'Himmel' erscheint",str(occurrences1)+"-mal.")

occurrences2 = data.count("Freiheit")
print("Das Wort 'Freiheit' erscheint",str(occurrences2)+"-mal.")

occurrences3 = data.count("Spiel")
print("Das Wort 'Spiel' erscheint",str(occurrences3)+"-mal.")

occurrences4 = data.count("Tanz")
print("Das Wort 'Tanz' erscheint",str(occurrences4)+"-mal.")