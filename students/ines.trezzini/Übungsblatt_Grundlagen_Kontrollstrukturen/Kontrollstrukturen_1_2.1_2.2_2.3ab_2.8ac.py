# Übungsblatt Grundlagen Kontrollstrukturen

# 1: Schreiben Sie ein Programm, welches eine ganze Zahl mittels input einliest und diese mit Zwei multipliziert.

# Variante 1:
zahl=input("Bitte geben Sie eine Zahl ein ...")
zahl=int(zahl)
print (zahl*2)

# Variante 2:
zahl=input("Bitte geben Sie eine Zahl ein ...")
print (int(zahl)*2)

# 2.1: Schreiben Sie eine Funktion guten morgen, welche den Text Guten Morgen! auf dem Bildschirm ausgibt.

def guten_morgen():
    print ("Guten Morgen!")

guten_morgen()

# 2.2: Schreiben Sie eine Funktion, welche als Parameter einen Namen entgegennimmt und anschliessend den Text Guten Morgen Name! ausgibt.

def guten_morgen(name):
    print ("Guten Morgen",name+"!")

guten_morgen("Ana")

# 2.3a): Schreiben Sie eine Funktion, welche einen Namen entgegennimmt und anschliessend den Text Guten Morgen Name! zurückgib.

def guten_morgen(name):
    gruss="Guten Morgen "+name+"!"
    return gruss

gruss = guten_morgen("Ana")
print(gruss)

# 2.3b): Schreiben Sie eine Funktion, welche die Länge und Breite eines Rechtecksentgegennimmt und die Fläche des Rechtecks an das Programm zurückgibt.

# Variante 1 (nicht streng nach Aufgabenblatt):
def flaeche_rechteck(länge, breite):
    flaeche = länge*breite
    print("Die Fläche beträgt", flaeche,"m2.")
    
flaeche_rechteck(10,20)

# Variante 2:
def flaeche_rechteck(länge, breite):
    flaeche = länge*breite
    return flaeche

flaeche = flaeche_rechteck(10, 20)
print("Die Fläche beträgt", flaeche, "m2.")

# 2.8a): Schreiben Sie eine Schleife, welche von 0 bis 10 zählt.
counter = 0
while counter <= 10:
    print(counter)
    counter = counter+1

# 2.8c): Schreiben Sie eine Schleife, welche von 10 bis 0 in Dreierschritten zählt.
# Problem: von 10 kommt man in Dreierschritten nicht bis 0, sondern -2 ...
counter = 10
while counter >= -2:
    if counter==-2:
        print(0)
    else:
     print(counter)
    counter = counter-3