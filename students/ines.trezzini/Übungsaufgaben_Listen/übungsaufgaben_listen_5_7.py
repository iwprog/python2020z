#  Übungsbeispiele Listen

# Beispiel 5
l_input = "Heinz war heute in den Bergen. Es war eine lange Wanderung"
stopwords = ('der', 'die', 'das', 'in', 'auf', 'unter',
'ein', 'eine', 'ist', 'war', 'es')

def stopword_filter(eingabe,stopwords):
    ohne_stopwörter = []
    eingabe=eingabe.lower()
    eingabe=eingabe.split()
    for wort in eingabe:
        if wort not in stopwords:
            ohne_stopwörter.append(wort)
    print(ohne_stopwörter) 

stopword_filter(l_input, stopwords)

# Variante:
l_input = "Heinz war heute in den Bergen. Es war eine lange Wanderung"

stopwords = ("der", "die", "das", "in", "auf", "unter", "ein", "eine",
             "ist", "war", "es", "den")

def stopword_filter(l_input, stopwords):
    l_input_neu = []
    for word in l_input.split(): # String in eine Liste umwandeln durch .split
        if word not in stopwords and word.lower() not in stopwords:
            l_input_neu.append(word)
    return l_input_neu
# bei return darf man keine Funktion einfügen
        
print(stopword_filter(l_input,stopwords))


# Beispiel 7
l_input= ['Gesundheit', 'Wanderung', 'Gesundheit', 'Gewandtheit', 'Wanderung']

def unique(eingabe):
    ohne_duplikate = []
    for wort in eingabe:
        if wort not in ohne_duplikate:
            ohne_duplikate.append(wort)
    print(ohne_duplikate) 

unique(l_input)

# Variante 1:
l_input = ["Gesundheit", "Wanderung", "Gesundheit", "Gewandtheit", "Wanderung"]

def removedup(l_input):
  dictionary = list(dict.fromkeys(l_input))
  return dictionary

print(removedup(l_input))
#Liste zu Dictionary umgewandelt, weil dabei Duplikate automatisch entfernt werden

# Variante 2:
l_input="Gesundheit Wanderung Gesundheit Gewandtheit Wanderung Wanderung Wanderung".split()

kopie=list(l_input)
for wort in kopie:
    if l_input.count(wort)>1:
        l_input.remove(wort)

print(l_input)

# Variante 3:
l_input = "Gesundheit Wanderung Gesundheit Gewandtheit Wanderung".split()
# Variante (schon als Liste): l_input= ['Gesundheit', 'Wanderung', 'Gesundheit', 'Gewandtheit', 'Wanderung']

def unique(l_input):
    resultat = []
    for word in l_input:
        if word not in resultat:      # Variante: if resultat.count(word) == 0:
            resultat.append(word)   
    return resultat

print(unique(l_input))

# Meine Variante von Variante 3:
l_input= ['Gesundheit', 'Wanderung', 'Gesundheit', 'Gewandtheit', 'Wanderung']

def unique(eingabe):
    ohne_duplikate = []
    for wort in eingabe:
        if wort not in ohne_duplikate:
            ohne_duplikate.append(wort)
    print(ohne_duplikate) 

unique(l_input)