#2.1 Dictionaries erstellen und manipulieren
#a
preisliste={"Brot":3.2,"Milch":2.1,"Orangen":3.75,"Tomaten":2.2}
#b
preisliste["Milch"]=2.05
#c
preisliste.pop("Brot")
#d
preisliste.update({"Tee":4.2,"Peanuts":3.9,"Ketchup":2.1})

#e
while True:
    lebensmittel=input("Bitte Lebensmittel eingeben: ")
    if lebensmittel in preisliste:
        print(lebensmittel,"ist bereits in der Preisliste.")
        exists=input("Preis ändern(y) oder abbrechen(x)?")
        if lebensmittel=="x":
            break
    if lebensmittel=="x":
        break
    else:
        print("Geben Sie bitte den Preis von",lebensmittel,"ein: ")
        preis=input()
        preisliste.update({lebensmittel:preis})
print(preisliste)


#2.2a
for lebensmittel,preis in preisliste.items():
    print(lebensmittel,":",preis)
#e
liste=list(preisliste.items())
liste.sort(key=lambda x:x[1])

#2.3 c. 
for lebensmittel in list(preisliste):
    if "en" in lebensmittel:
        preisliste.pop(lebensmittel)
print(preisliste)
        

# b) übungsbeispiele zu listen (lists.pdf)
# beispiel 5: remove stopwords

import re
l_input="Heinz war heute in den Bergen. Es war eine \ lange Wanderung"
lazy="der die das in auf unter ein eine ist war es"
stoppworte=re.sub("[^\w]", " ",lazy).split()

def stoppwort_filter(l_input, stopword):
    string=re.sub("[^\w]", " ",l_input).split()
    for word in string:
        if word in stopword:
            string.remove(word)
            output=[item.lower() for item in string]
    print(output)

stoppwort_filter(l_input,stoppworte)

           
#beispiel 7: unique (entfernen von doppelten einträgen)
still_lazy="Gesundheit Wanderung Gesundheit Gewandtheit Wanderung"
l_input=re.sub("[^\w]", " ",still_lazy).split()
print(l_input)

for wort in l_input:
    if l_input.count(wort)==1:
        l_input.remove(wort)

print(l_input)

# c) übungsbeispiele zu dictionaries (dicts.pdf)
#beispiel 2: wortstatistik
still_lazy="Der Tag begann sehr gut! Der morgen war schön.".lower()
l_input=re.sub("[^\w]", " ",still_lazy).split()

ausgabe={}  

for wort in l_input:
    ausgabe[wort]=l_input.count(wort)
print(ausgabe)


#3: lösung 1: icao alphabet
import phonetic_alphabet as alpha
wort=input("Welches Wort soll ich buchstabieren? ")
print(alpha.read(wort))


#3: lösung 2: icao alphabet
import string
liste="Alpha Bravo Charlie Delta Echo Foxtrot Golf Hotel India Juliett Kilo Lima Mike November Oscar Papa Quebec Romeo Sierra Tango Uniform Victor Whiskey Xray Yankee Zulu"
iaco_w=re.sub("[^\w]", " ",liste).split()
iaco_abc=list(string.ascii_lowercase)
print(iaco_abc)
print(iaco_w)
dict={}
for i in range(len(iaco_abc)):
        dict[iaco_abc[i]]=iaco_w[i]

eingabe=list(input("Welches Wort soll ich buchstabieren?").lower().replace("ä","ae").replace("ö","oe").replace("ü","ue"))
ausgabe=[]
for buchstabe in eingabe:
    ausgabe.append(dict[buchstabe])
print("-".join(ausgabe))

#ad hoc übung 3.3 c
orte={"Zürich":370000,"Basel":200000,"Bern":120000,"Biel":55000,"Berlin":2500000}
l_orte=list(orte.items())
l_orte.sort(key=lambda x:x[1],reverse=True)

print(l_orte)
for stadt,zahl in l_orte:
    print(stadt,zahl)


