# # a.
# 
# def legal_status(age):
#     if age < 18:
#         return("minderjährig")
#         
#     else:
#         return("volljährig")
#         
# age = 17
# 
# print("Mit", age, "ist man", legal_status(age) + ".")


# b.

def legal_status(age):
    if age < 0:
        return("ungeboren")
    
    elif age <= 6:
        return("geschäftsunfähig")
    
    elif age <= 14:
        return("unmündig")
    
    elif age < 18:
        return("mündig minderjährig")
        
    else:
        return("volljährig")
        
age = -6

print("Mit", age, "ist man", legal_status(age) + ".")