#Übungsblatt Datentypen
#2.1
#a)
preisliste={"Brot": 3.2, "Milch":2.1, "Orangen":3.75, "Tomaten":2.2}
print(preisliste)

#b)
preisliste["Milch"]=2.05
print(preisliste)

#c)
del preisliste["Brot"]
print(preisliste)

#d)
preisliste["Tee"]=4.2
preisliste["Peanuts"]=3.9
preisliste["Ketchup"]=2.1
print(preisliste)

#e)
def lebensmittel():
    lebensmittel={}
    while True:
        abfrage= input("Welches Lebensmittel soll zur Liste hinzugefügt werden? (x für Exit): ")
        if abfrage == "x":
            break
        preis = input ("Preis des Lebensmittel? : ")
        lebensmittel[abfrage]=preis
    return lebensmittel
print(lebensmittel())

