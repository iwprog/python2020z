#Leistungsaufgabe 3
#a)
liste=[]

while True: 
    name=input("Geben Sie den Namen der Stadt ein: ")
    if name == "x":
        break
    einwohnerzahl=input("Geben Sie die zugehörige Einwohnerzahl ein: ")
    liste.append([name, einwohnerzahl])

print(sorted(liste))

#b)
woerterbuch={
  "Haus":"house", 
  "Zahnarzt":"dentist", 
  "Zahn":"dent", 
  "Blume":"flower", 
  "Baum": "tree", 
  "Sonne":"Sun", 
  "Auto": "car", 
  "Schule":"school", 
  "Hund": "dog", 
  "Katze":"cat" 
  }
statistik=[]


for deutsch in woerterbuch:
  test=input("Was heisst " + deutsch + " : ")
  if test == woerterbuch[deutsch]:
    statistik.append("Richtig")
    print("RICHTIG!")
  else:
    print("FALSCH")

print("Sie haben ", len(statistik), " von 10 Vokabeln richtig beantwortet! ")
