#2.5 a)
# Funktion die Alter entgegennimmt + je nach Alter den Wert volljährig oder minderjährig zurückgibt
def legal_status():
    alter=int(eingabe)
    if alter >= 18:
        status = "volljährig"
    elif alter < 18:
        status = "minderjährig"
    return print("Mit", alter, "ist man", status + ".")

eingabe=input("Wie alt ist die Person?")
print(legal_status())