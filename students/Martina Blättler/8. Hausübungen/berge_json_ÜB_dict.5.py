from json import loads, dumps
berge={}
for x in range (1,4):
    berg_eingabe=input(str(x) + ". Berg?")
    höhe_eingabe= input(str(x) + ". Höhe?")
    höhe_eingabe = int(höhe_eingabe)
    berge[berg_eingabe]=höhe_eingabe
    
with open("berge.json", 'w', encoding='utf8') as datei:
    json_string = dumps(berge)
    datei.write(json_string)
try:
    with open("berge.json", "r", encoding="utf8") as datei_2:
        json_string = datei_2.read()
        berge_ausgabe = loads(json_string)
    print(berge_ausgabe)

except FileNotFoundError:
    print ("Das Dokument 'berge.json' konnte nicht gefunden werden")