def menü():
    print ("***********MENÜ*******************")
    print("(1) artikel hinzufügen") #funktioniert nicht richtig
    print("(2) artikel löschen")
    print("(3) artikel suchen") #funktioniert nicht
    print("(4) einkaufsliste leeren")
    print("(5) einkaufsliste speichern")
    print("(6) einkaufsliste laden")
    print("(7) einkaufsliste im csv format exportieren")
    print("(0) exit")
    print("--------------------------------------------------")

from json import loads, dumps
from csv import writer

einkaufsliste = []
artikel_dic={}
while True:
    artikel_details=[]
    menü()
    auswahl = input("Was möchten Sie ausführen?")
    if auswahl == "1":
        artikel = input("Welchen Artikel möchten Sie hinzufügen?")
        preis = input ("Wie viel kostet der Artikel?")
        menge = input("Wie oft soll dieser auf die Liste?")
        artikel_details.append(preis)
        artikel_details.append(menge)
        artikel_dic[artikel]=artikel_details
        einkaufsliste.append(artikel_dic)
        print(einkaufsliste)
        print(artikel_dic.items())
        
    elif auswahl == "2":
        artikel = input("Welchen Artikel möchten Sie löschen?")
        del artikel_dic[artikel]
        print(einkaufsliste)
        
    elif auswahl == "3":
        suchbegriff = input("Nach welchem Suchbegriff möchten Sie suchen?")
        for i in einkaufsliste:
            if suchbegriff in i:
                print ("Gefunden:", i)
        
    elif auswahl == "4":
        artikel_dic = {}
        einkaufsliste=[]
    elif auswahl == "5":
        with open('einkaufsliste_menü.json', 'w', encoding='utf8') as f:
            json_string = dumps(einkaufsliste)
            f.write(json_string)
    elif auswahl == "6":
        with open('einkaufsliste_menü.json', encoding='utf8') as f:
            json_string = f.read()
            ausgabe = loads(json_string)
            print(ausgabe)
    elif auswahl == "7":
        with open('einkaufsliste_menü.csv', 'w', encoding='utf8') as f:
            csv_writer = writer(f, delimiter=';')
            for item,worth in artikel_dic.items():
                csv_writer.writerow([item,worth])
    elif auswahl == "0":
        break
    else:
        print ("Falsche Eingabe")
        
    