#sortiert nach Höhe geht noch nicht --> wie kann ich nach zahlen sortieren mit sorted?
berge_dict={}
gebirge_dict={}
sortierliste =[]
for m in range (3):
    berg=input("Welchen Berg möchten Sie erfassen:?")
    höhe= input("Wie hoch ist dieser in Meter?")
    berge_dict[berg]=höhe
    gebirge=input("Zu welchem Gebirge gehört der Berg?")
    gebirge_dict[berg]=gebirge

#erstellt ein Dictionary mit Bergen & Höhen und ein Dictionary mit Bergen & Gebirge


print("------------------------")
x=0
y = 0
z=0
sortier_wunsch=input("Sortieren nach (1) Berg, (2) Höhe oder (3) Gebirge):")
if sortier_wunsch == "1":
    for mountain, height in berge_dict.items():
        sortierliste.append([mountain,height])
    for mountain, mountain_range in gebirge_dict.items():
        sortierliste[x].extend([mountain_range])
        x=x+1
    reihenfolge = input("Nach Berg (1) aufsteigend, oder (2) absteigend sortieren?")
    print("********************************************************")
    if reihenfolge =="1":
        for mountain, height,mountain_range in sorted(sortierliste):
            print (mountain, "ist", height, "m. und gehört zum",mountain_range, "Gebirge")
    elif reihenfolge =="2":
        for mountain, height,mountain_range in sorted(sortierliste, reverse = True):
            print (mountain, "ist", height, "m. und gehört zum",mountain_range, "Gebirge")
        
        
elif sortier_wunsch=="2": #nach Höhe --> funktioniert noch nicht, wie kann ich nach zahlen sortieren?
    for mountain, height in berge_dict.items():
        sortierliste.append([height,mountain])
    for mountain, mountain_range in gebirge_dict.items():
        sortierliste[y].extend([mountain_range])
        y=y+1
    reihenfolge = input("Nach Höhe (1) aufsteigend, oder (2) absteigend sortieren?")
    print("********************************************************")
    if reihenfolge =="1":
        for height, mountain, mountain_range in sorted(sortierliste):
            print (mountain, "ist", height, "m. und gehört zum",mountain_range, "Gebirge")
    elif reihenfolge =="2":
        for height, mountain, mountain_range in sorted(sortierliste, reverse=True):
            print (mountain, "ist", height, "m. und gehört zum",mountain_range, "Gebirge")
            
            
elif sortier_wunsch =="3":
    for mountain,mountain_range in gebirge_dict.items():
        sortierliste.append([mountain_range,mountain])
    for mountain, height in berge_dict.items():
        sortierliste[z].extend([height])
        z=z+1
    reihenfolge = input("Nach Gebirge (1) aufsteigend, oder (2) absteigend sortieren?")
    print("********************************************************")
    if reihenfolge =="1":
        for mountain_range,mountain, height in sorted(sortierliste):
            print (mountain, "ist", height, "m. und gehört zum",mountain_range, "Gebirge")
    elif reihenfolge =="2":
        for mountain_range,mountain, height in sorted(sortierliste, reverse = True):
            print (mountain, "ist", height, "m. und gehört zum",mountain_range, "Gebirge")
    