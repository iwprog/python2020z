#2.1 a
preisliste = {"brot":3.2,"milch":2.1,"orangen":3.75,"tomaten":2.2}

#2.1 b
preisliste["milch"]=2.05
print(preisliste)

#2.1 c
del preisliste["brot"]
print(preisliste)

#2.1 d
preisliste["tee"]=4.2
preisliste["peanuts"]=3.9
preisliste["ketchup"]=2.1
print(preisliste)

#2.1 e
lebensmittel_liste={}
lebensmittel = ""
while lebensmittel != "x":
    lebensmittel = input("Welches lebensmittel möchten sie hinzufügen? Sonst x")
    if lebensmittel =="x":
        break
    else:
        preis = input("Wie viel kostet es?")
        lebensmittel_liste[lebensmittel]=preis
print(lebensmittel_liste.items())

#2.1f
zahlen_lebensmittel_liste={}
zahlen_lebensmittel = ""
while zahlen_lebensmittel != "x":
    zahlen_lebensmittel = input("Welches lebensmittel möchten sie hinzufügen? Sonst x")
    if zahlen_lebensmittel =="x":
        break
    else:
        zahlen_preis = float(input("Wie viel kostet es?"))
        zahlen_lebensmittel_liste[zahlen_lebensmittel]=zahlen_preis
print(zahlen_lebensmittel_liste.items())
        
    