preisliste = {"Orangen":3.75,"Brot":3.2,"Milch":2.1,"Tomaten":2.2, "Senf":1.50, "Zwiebel": 0.50, "Banane": 0.90 , "Ketchup": 2.1}
preisliste_verändert={"Orangen":3.75,"Brot":3.2,"Milch":2.1,"Tomaten":2.2, "Senf":1.50, "Zwiebel": 0.50, "Banane": 0.90 , "Ketchup": 2.1}
#es braucht eine kopie des dictionaries, da sich dieses während der for Schleife verändert (gibt Fehler)
#so wird die originale Liste in der for Schleife nicht verwendet, aber die korrekten lebensmittel werden davon gelöscht

for lebensmittel,preis in preisliste_verändert.items():
    for x in range(10):
        if lebensmittel[x:x+2] == "en":
            del preisliste[lebensmittel]
            
print (preisliste)