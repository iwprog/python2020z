from turtle import*

#Eingebefenster
size=numinput("Seitenlänge", "Bitte Seitenlänge angeben")
#Rundung von size auf ganze Zahlen
size=int(size)

#roter Hintergrund fürs Kreuz
pencolor("red")
fillcolor("red")
begin_fill()
forward(size*5)
left(90)
forward(size*5)
left(90)
forward(size*5)
left(90)
forward(size*5)
end_fill()

#stift umplatzieren
penup()
left(90)
forward(size*2)
left(90)
forward(size)

#weisses Kreuz
pendown()
pencolor("white")
begin_fill()
fillcolor("white")
forward(size)
left(90)
forward(size)
right(90)
forward(size)
right(90)
forward(size)
left(90)
forward(size)
right(90)
forward(size)
right(90)
forward(size)
left(90)
forward(size)
right(90)
forward(size)
right(90)
forward(size)
left(90)
forward(size)
end_fill()

#stift 'verstecken'
ht()


