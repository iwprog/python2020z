#1.2c
from turtle import *

reset()
pensize(5)
pencolor("blue")
left(45)
forward(100)
right(90)
pencolor("red")
forward(100)
left(90)
pencolor("cyan")
forward(100)
right(90)
pencolor("black")
forward(100)