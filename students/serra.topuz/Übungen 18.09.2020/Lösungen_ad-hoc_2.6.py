#Übung 2.6 / Drei Vierecke
from turtle import*
reset()

def viereck(seitenlaenge, fuellfarbe):
    fillcolor(fuellfarbe)
    
    begin_fill()
    fd(seitenlaenge);lt(90);fd(seitenlaenge);lt(90);fd(seitenlaenge);lt(90);fd(seitenlaenge)
    end_fill()
    
pencolor("red")
pensize(5)

viereck(200, "yellow")
viereck(200, "cyan")
viereck(200, "lime")


#Übung 2.6 / Erweiterung Vierecke
from turtle import*
reset()

i = 0
while i < 4:
    fd(200);lt(90)
    i = i + 1
    

