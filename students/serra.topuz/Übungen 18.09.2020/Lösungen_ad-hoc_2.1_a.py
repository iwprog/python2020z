#Übung 2.1 a)
from turtle import*
reset()

#Funktionen Dreieck
def dreieck(seitenlaenge, fuellfarbe):
    fillcolor(fuellfarbe)
    
    begin_fill()
    rt(120);fd(seitenlaenge);lt(120);fd(seitenlaenge);lt(120);fd(seitenlaenge)
    end_fill()
    
#Generelle Formate
pencolor("red")
pensize(5)
right(90)

#Dreiecke
dreieck(200, "yellow")
dreieck(200, "cyan")
dreieck(200, "lime")



