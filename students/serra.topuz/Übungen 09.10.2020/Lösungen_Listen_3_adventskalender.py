# Übungsbeispiele: Listen 3. Adventskalender

from random import choice

adventskalender = ["Samiklaus", "Christbaum", "Weihnachtskugel", "Stiefel", "Schneeball"]

eingabe = 50

while eingabe !="x" : 
    eingabe = input("Welche Kalendertur wollen Sie offnen (oder x fur Exit): ")
    if eingabe in range(1,25):
        print(choice(adventskalender))
    else:
        break
    
# weiter geht es nicht mehr