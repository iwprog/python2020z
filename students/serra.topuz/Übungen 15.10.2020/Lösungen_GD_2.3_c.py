# Grundlagen Datentypen: 2.3 Dictionaries filtern

# c) Lebensmittel rausfiltern: "en"
preisliste = {"Brot": 3.2, "Milch": 2.1,
              "Orangen": 3.75,
              "Tomaten": 2.2, "Tee": 4.2,
              "Peanuts": 3.9, "Ketchup": 2.1}

neu_preisliste = {} # neue leere Preisliste/Dictionary

for key, value in preisliste.items():
    if key == "en":
        neu_preisliste[key] = value
        
print(neu_preisliste)

# oder?

def suffix_preisliste (preisliste):
    neu_preisliste = {}
    for key, value in preisliste:
        if preisliste.endswith("en"):
            del preisliste[key]
        else:
            neu_preisliste[key] = value
            
print(neu_preisliste)
