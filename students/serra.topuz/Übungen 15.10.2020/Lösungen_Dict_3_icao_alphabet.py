# Übungsbeispiele Dictionaries: 3. ICAO-Alphabet

eingabe = input("Welches Wort soll ich buchstabieren?: ")

d = {'a':'alfa', 'b':'bravo', 'c':'charlie', 'd':'delta', 'e':'echo', 
     'f':'foxtrot', 'g':'golf', 'h':'hotel', 'i':'india', 'j':'juliett', 
     'k':'kilo', 'l':'lima', 'm':'mike', 'n':'november', 'o':'oscar', 
     'p':'papa', 'q':'quebec', 'r':'romeo', 's':'sierra', 't':'tango', 
     'u':'uniform', 'v':'victor', 'w':'whiskey', 'x':'x-ray', 'y':'yankee', 
     'z':'zulu'}

for i in eingabe:
    if i.lower() in d.keys():
        print("Ausgabe:", d[i.lower()])
    else:
        print("Ausgabe:", i)


# a) weiter ging es nicht

for i in eingabe:
    if i.lower() in d.keys():
        print("Ausgabe:", d[i.lower()])
    elif i == "ü":
        print("Kann Übung nicht buchstabieren, da 'Ü' nicht definiert wurde.")
    else:
        print("Ausgabe:", i)
        
# b) weiter ging es nicht
    