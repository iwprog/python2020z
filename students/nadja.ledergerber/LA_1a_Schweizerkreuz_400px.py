from turtle import *
reset()

hideturtle()
speed(10) #standard speed is too slow!

#Hintergrund
pencolor("red")
pensize(1)

fillcolor("red")
begin_fill()
left(180)
forward (400/2)
left(90)
forward (400)
left(90)
forward (400)
left(90)
forward (400)
left(90)
forward (400/2)
end_fill()

penup() #hebe Stift an
left(90)
forward(400/5)
pendown() #setze Stift wieder ab

#Kreuz

a = 400/5 #variable macht schreiben schneller

pencolor("white")
fillcolor("white")
begin_fill()

right(90)
forward(a/2)
left(90)
forward(a)
right(90)
forward(a)
left(90)
forward(a)
left(90)
forward(a)

right(90)
forward(a)
left(90)
forward(a)
left(90)
forward(a)

right(90)
forward(a)
left(90)
forward(a)
left(90)
forward(a)

right(90)
forward(a)
left(90)
forward(a/2)

end_fill()
