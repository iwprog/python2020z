# 2. Schreiben Sie eine Funktion, welche das Alter einer Person entgegennimmt
# und daraus die theoretische maximale Herzfrequenz berechnet.
# Die Formel fur die Berechnung der Herzfrequenz lautet:
# max Herzfrequenz = 220 - Alter in Jahren
# >>>berechne_herzfrequenz(25)
# 195

from turtle import *

def berechne_herzfrequenz():
    Alter = int(numinput("Alter", "Geben Sie das Alter ein"))
    print(220 - Alter)

berechne_herzfrequenz()

