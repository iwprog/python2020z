#
# Schreiben Sie eine Funktion, die das Volumen eines Zylinders berechnet.
#  V = r^2 * pi * H
#

import math

def berechne_volumen(radius, hoehe):
    volumen = math.pi * radius**2 * hoehe
    return volumen

# option 1: direkte ausgabe des ergebnisses
print("Das Volumen einens Zylinders mit Radius 2 und Höhe 3 ist", berechne_volumen(2, 3))

# option 2: ergebnis zuerst in eine variable speichern
volumen = berechne_volumen(2, 3)
print("Das Volumen einens Zylinders mit Radius 2 und Höhe 3 ist", volumen)

