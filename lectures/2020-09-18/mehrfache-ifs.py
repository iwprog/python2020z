a = input("Bitte Wert eingeben...")

if a == "1":
    print("Eins")
if a == "2":
    print("Zwei")
if a == "3":
    print("Drei")
if a != 1 and a != 2 and a != 3:
    print("Falsch")
    
## Versus
    
if a == "1":
    print("Eins")
elif a == "2":
    print("Zwei")
elif a == "3":
    print("Drei")
else:
    print("Falsch")