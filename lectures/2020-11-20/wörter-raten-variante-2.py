# Übungsbeispiel: Wörter raten
# Gegeben: ein Wort + eine Vorschrift anhand welcher Selbstlaute vertauscht werden

# z.B.
#  a -> o, o -> e, i - >u, u ->a, e->i
#
# apfel -> opfil
#
# Gegeben ist das Wort: opfil
# Raten Sie wie das ursprüngliche Wort hiess: upfil
# -> Falsch, bitte versuchen Sie es erneut...

wort = "wacholder"

ergebnis = []
ratewort = ""
for buchstabe in wort:
    if buchstabe == "a":
        buchstabe = "o"
    elif buchstabe == "o":
        buchstabe = "e"
    elif buchstabe == "e":
        buchstabe = "i"
    elif buchstabe == "i":
        buchstabe = "u"
    elif buchstabe == "u":
        buchstabe = "a"
        
    ergebnis.append(buchstabe)

ratewort = "".join(ergebnis)
print(ratewort)

