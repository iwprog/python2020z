from csv import writer, reader



# try/except

try:
    l = []
    # open("liste2.csv", "r", newline = '', encoding="utf8") as f:
    with open("liste2.csv", "r", encoding="utf-8") as f:
        csv = reader(f, delimiter=";")
        for item, wert in csv:
            wert = float(wert)
            l.append((item, wert))
            
        # for zeile in csv:
        #     item = zeile[0]
        #     wert = zeile[1]
        #     item, wert = zeile
except FileNotFoundError:
    print("Keine Sicherungsdatei gefunden - starte von 0.")
    l = []

print(l)
