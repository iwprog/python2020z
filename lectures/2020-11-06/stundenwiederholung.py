#
# Stundenwiederholung vom 6. November 2020
#

# Variante 1
text = "Heute ist ein schöner Tag...."
f = open("text.txt", "w", encoding="utf8")
f.write(text)
f.close()

# Variante 2
with open("text.txt", "w", encoding="utf8") as f:
    f.write(text)
    f.write("\n")
    f.write("Zeile 2\n")
    f.write("Zeile 3\n")
    
#f.write("Zeile 4")
f = open("text.txt", "r", encoding="utf8")
text2 = f.read()
print(text2)
f.close()

print("----\nVariante 2\n-----")
with open("text.txt", encoding="utf8") as f:
    text3 = f.read()
    
print(text3)
