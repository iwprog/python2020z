from csv import reader

try:
    einkaufsliste = []
    with open("einkaufsliste.csv", encoding="utf8") as f:
        csv_reader=reader(f,delimiter=";")
        for row in csv_reader:
            print("Einkaufsliste:"+", ".join(row))
            einkaufsliste.append(row)

except FileNotFoundError:
    einkaufsliste = []
    
print(einkaufsliste)
