# Beispiel zum Einlesen von Web Ressourcen

# Aufgabe:
#  (1) Lesen Sie den Text von https://www.gutenberg.org/ebooks/6079.txt.utf-8 ein
#  (2) Zählen Sie wie oft dort die Worte "Winter", "Schnee", "kalt" vorkommen.
#
from urllib.request import urlopen

with urlopen('https://www.gutenberg.org/ebooks/6079.txt.utf-8') as source:
        web_content = source.read().decode('utf8')
        print(web_content)
        
        wörter = web_content.split()
        print(wörter.count("Winter"))
        print(wörter.count("Schnee"))
        print(wörter.count("kalt"))
            
        print(web_content.count("Winter"))
        print(web_content.count("Schnee"))
        print(web_content.count("kalt"))
        