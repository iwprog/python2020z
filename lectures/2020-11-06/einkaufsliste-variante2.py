# Aufgabe:
#  (1) Speichern Sie die folgende Einkaufsliste im CSV Format ab (Datei: Einkaufsliste.csv)
#  (2) probieren Sie die Datei in Excel zu öffnen
#  (3) Erweitern Sie Ihr Programm, sodass dieses
#       (a) die Einkaufsliste einliesst, wenn die Datei Einkaufsliste.csv existiert; ansonsten soll die Liste leer sein.
#       (b) die List im Anschluss in einer Schleife ausgibt.

einkaufsliste = [["Eis", 2.33, 2],
                 ["Eier", 0.33, 10],
                 ["Äpfel", 3.99, 2]]

from csv import *

with open('einkaufsliste.csv', 'w', encoding='utf8') as einkaufsliste_:
    csv_writer = writer(einkaufsliste_, delimiter=';')
    for item in einkaufsliste:
        csv_writer.writerow(item)



