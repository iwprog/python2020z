# Anna Hilgert
from turtle import*
reset()

def nachInnen(breite):
    penup()
    fd(breite)
    lt(90)
    fd(breite)
    rt(90)
    pendown()
    
def quadrat(a):
    i=1
    while i<=4:
        fd(a)
        lt(90)
        i=i+1    


def zeichne_rahmen(länge, breite, rahmenfarbe, füllfarbe):
    pencolor(rahmenfarbe)
    fillcolor(füllfarbe)
    begin_fill()
    quadrat(länge)
    end_fill()

    nachInnen(breite)

    fillcolor("white")
    begin_fill()
    quadrat(breite * 2)
    end_fill()

zeichne_rahmen(100,25,"blue","green")
