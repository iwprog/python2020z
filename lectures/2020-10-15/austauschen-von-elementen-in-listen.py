liste = ["Ana", "Toni", "Jim", "Ursula", "Anita"]

# Aufgabe: Tauschen Sie alle Namen, die mit einem Selbstlaut (AEIOU) beginnen durch
# den Namen "Julius" aus.

# 5 min bis 13:40 - 13:45

# Variante 1
liste4 = ["Ana", "Toni", "Jim", "Ursula", "Anita"]
for element in liste4:
    if element[0] == 'A' or element[0] == 'E' or element[0] == 'I' or element[0] == 'O' or element[0] == 'U':
        index = liste4.index(element)
        liste4[index] = 'Julius'
        
print(liste4)
    
# Variante 2
liste = ["ana","toni","emil","jim","ursula","anita"]
x = 0
for name in liste:
    if name.startswith(("a", "e", "i", "u", "o")):
        liste[x] = "Julius"
    x = x+1
print(liste)

# Variante 3
liste=["Ana","Toni","Jim","Ursula","Anita"]

for x, name in enumerate(liste):
  if name.startswith(("A","E","I","O","U")):
    liste[x]="JULIUS!!!!"

print(liste)


# exkurs != und or
name = "ana"
if name[0] != "a" or name[0] != "e":
    print("Hallo")
    
# Pause bis 14:11
