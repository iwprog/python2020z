personen = {"ana":37, "tim":24, "susi":13}


sortierliste = []
for name, alter in personen.items():  # [('ana', 37), ('tim', 24), ('susi', 13)]
    sortierliste.append([alter, name])    
print(sortierliste)

for alter, name in sorted(sortierliste):
    print(name, "ist", alter, "alt.")