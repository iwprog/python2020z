ICAO={"a":"Alfa", "b":"Bravo", "c":"Charlie", "d":"Delta",
      "e":"Echo","f":"Foxtrot","g":"Glof","h":"Hotel", "i":"India",
      "j":"Juliett", "k":"Kilo", "l":"Lima","m":"Mike",
      "n":"November", "o":"Oscar", "p":"Papa", "q":"Quebec",
      "r":"Romeo","s":"Sierra","t":"Tango","u":"Uniform","v":"Victor",
      "w":"Whiskey","x":"X-Ray","y":"Yankee","z":"Zulu"}

#3.0
wort = input("Welches Wort soll ich buchstabieren?")
wort = wort.lower().replace("ä","ae").replace("ö","oe").replace("ü","ue")

ergebnis = []
for position, x in enumerate(wort):
    ergebnis.append(ICAO[x])

# join befehl: Trennzeichen (z.b. '-') und im Anschluss eine Liste von
# Elementen, die "gejoined" werden soll.
print("-".join(ergebnis))

