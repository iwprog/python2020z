s = "Heute ist ein schöner Tag"

a = 5*12
print(5*12)
print(len(s))

print(list(enumerate(s, 1)))

for no, buchstabe in enumerate(s, 1):
    print(no, buchstabe)

# Konvertierung zwischen Datentypen
a = 0x64
s = "@"
f = 1.1E100

s = "170.9"
f = float(s)
f_gerundet = round(f, 0)
i = int(f_gerundet)
print(f_gerundet)
print(i)

print(round(f,-1))

# Stringbefehle

# (1) count
s = "la, la, la o la..."

print(s.count("la"))
print(s.find("la"))
print(s.find("la", 1))

# la, la, la o la
# 000000000011111
# 012345678901234

s = "la, la, la o la..."
needle = "la"
for index, buchstabe in enumerate(s):
    if needle == s[index:index+len(needle)]:
        print("Found at index", index)
    
found = 0
while found != -1:
    found = s.find(needle, found)
    if found != -1:
        print(found)
        found = found + 1

print(s.replace("la", "li"))
