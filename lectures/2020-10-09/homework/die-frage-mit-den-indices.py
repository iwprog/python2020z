städte = [ ["Zürich", "ZH", 370000],
           ["Genf", "GE", 190000],
           ["Basel", "BS", 170000],
           ["Bern", "BE", 130000] ]

while True:
    stadt = input("Stadt (oder 'x') für exit? ")
    if stadt == 'x':
        break
    
    kanton = input("Kanton? ")
    einwohner = int(input("Einwohner"))
    städte.append([stadt, kanton, einwohner])

ergebnis = []
for stadt, kanton, einwohner in städte:
    if einwohner > 150000:
        print(stadt, "hat", einwohner, "Einwohner.")
        ergebnis.append([stadt, kanton, einwohner])
        
print(ergebnis)

