# Unterscheidung zwischen Befehl und Variable

# die Klammer führt den Befehl aus (!)
p = 12
print(p)

print(">>>" + "  Ana Maria  ".strip() + "<<<")

# "Eingebaute Befehle" => kein "." ist notwendig
print("Hallo")

# Befehle die importiert wurden oder sich auf ein Objekt beziehen
# -> diese haben einen Punkt
a = " Julius von Toth "
print(a.swapcase())

