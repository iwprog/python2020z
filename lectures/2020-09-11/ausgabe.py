print(7*8)
print("Hallo", 100)

# print("hallo" + 100) funktioniert nicht (inkomp. datentypen)
print("hallo"  + str(100))

# String (text)
a = "Text"
b = 'Text'
c = """Text"""
d = '''Text'''
e = """Ana sagt: "Mit Gänsefüschen ist es schwierig..."."""

# Anführungszeichen fehlen => Computer glaubt, es ist eine Variable
# f = Ana

# ganze Zahl
a = 123

# Fliesskommazahl
b = 3.1
c = 123.

print("Guten Tag!"[3])
